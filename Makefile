.PHONY: all


runserver:
	.env/bin/python back/manage.py migrate
	.env/bin/python back/manage.py runserver

install:
	.env/bin/pip install -r requirements_local.txt


generate:
	.env/bin/python back/manage.py migrate
	.env/bin/python back/manage.py cities_light_fixtures load
	.env/bin/python back/manage.py reorder main.Country main.Region main.City
	.env/bin/python back/manage.py generate


reinstall:
	rm db/db.sqlite3
	.env/bin/pip install -r requirements_local.txt
	.env/bin/python back/manage.py migrate
	.env/bin/python back/manage.py cities_light_fixtures load
	.env/bin/python back/manage.py reorder main.Country main.Region main.City
	.env/bin/python back/manage.py generate


.PHONY: front
build-front: ## build static front content
	cd front && yarn && yarn build

build-front-fullscript: ## build static without minify
	cd front && yarn && yarn build-full-js
