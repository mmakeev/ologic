from django.apps import AppConfig


class EmailDeliveryConfig(AppConfig):
    name = 'email_delivery'

    def ready(self):
        super().ready()
        import email_delivery.signal_handlers
