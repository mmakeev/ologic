from django.contrib import admin
from project.admin import custom_admin_site
from .models import Delivery, DeliveryAccount, DeliveryItem, Email
from django.http import HttpResponseRedirect
from django.urls import reverse
from import_export import resources


class DeliveryResource(resources.ModelResource):

    class Meta:
        model = Delivery


@admin.register(Delivery, site=custom_admin_site)
class DeliveryAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ['name', 'delivery_account', 'created_at', 'updated_at',
                    'sent', 'not_sent', 'is_active']
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'initial_amount',
                       'status')

    change_form_template = 'admin/email_delivery/change_form_delivery.html'

    def not_sent(self, obj):
        return obj.get_delivery_item_qs().count()

    def response_change(self, request, obj):
        if "_send_emails" in request.POST:
            obj.send_butch_emails(request)
            return HttpResponseRedirect(reverse('admin:email_delivery_delivery_changelist'))
        return super().response_change(request, obj)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)


@admin.register(Email, site=custom_admin_site)
class EmailAdmin(admin.ModelAdmin):
    search_fields = ['subject']
    list_display = ['subject', 'created_at', 'updated_at']
    readonly_fields = ('created_at', 'updated_at')
    change_form_template = 'admin/email_delivery/change_form_email.html'

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = {'template_vars': self.model.template_vars}
        return super().add_view(request, form_url='', extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = {'template_vars': self.model.template_vars}
        return super().change_view(request, object_id, form_url='', extra_context=extra_context)

@admin.register(DeliveryItem, site=custom_admin_site)
class DeliveryItemAdmin(admin.ModelAdmin):
    list_display = ['delivery', 'user', 'email', 'sent_at', 'created_at',
                    'updated_at', 'is_sent']
    search_fields = ['email', 'delivery__name', 'user__email']
    readonly_fields = ('updated_at', 'sent_at')


@admin.register(DeliveryAccount, site=custom_admin_site)
class DelivereAccountAdmin(admin.ModelAdmin):
    list_display = ['name', 'user', 'service', 'is_active', 'created_at',
                    'updated_at']
    search_fields = ['name', 'user__username']
    readonly_fields = ('created_at', 'updated_at')
