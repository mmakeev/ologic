from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from .email_backends import TEMPATE_VARS, parse_vars_from_template


def validate_email_template_vars(value):
    vars = parse_vars_from_template(value)
    unknow_vars = list(set(vars) - set(TEMPATE_VARS))
    if unknow_vars:
        raise ValidationError(
            _('Template variable is missing %(unknow_vars)s'),
              params={'unknow_vars': str(unknow_vars)}
        )
