import json
import logging
import re
from abc import ABC, abstractmethod

import requests
from django.conf import settings
from django.db.models import F
from django.utils import timezone

logger = logging.getLogger(__name__)

TEMPLATE_VAR_PATTERN = getattr(settings, 'EMAILDELIERY_TEMPLATE_VAR_PATTERN',
                               r'\{\{\s*(?P<key>\w+)\s*\}\}')
template_var_pattern = re.compile(TEMPLATE_VAR_PATTERN)

DELIVERY_ITEM_VALUES_FOR_EMAIL_TEMPLATES = (
    ('email', 'email'),
    ('first_name', 'user__first_name')
)

TEMPATE_VARS = dict(DELIVERY_ITEM_VALUES_FOR_EMAIL_TEMPLATES).keys()
REVERSED_TEMPLATE_VARS = {v: k for k, v in dict(
    DELIVERY_ITEM_VALUES_FOR_EMAIL_TEMPLATES
).items()}


def parse_vars_from_template(template_string):
    """
    Парсит все вхождения {{ var }} в шаблоне и возвращает их списком
    """
    return template_var_pattern.findall(template_string)


class DeliveryEmailBackend(ABC):
    template_var_sub_pattern = None
    api_url = None
    delivery = None
    limit_batch_count = None

    def __init__(self, delivery, delivery_account, delivery_items_qs):
        # this is for re.sub() replace tags format {{ var }} to {% var %}
        self.template_var_sub_pattern = r'{% \1 %}'
        self.api_key = delivery_account.api_key
        self.delivery = delivery
        self.delivery_item_qs = delivery_items_qs.filter(delivery=delivery)
        self.test_mode = delivery_account.test_mode

    @abstractmethod
    def send_mail(self, delivery, delivery_item):
        """
        отправка одного емейла
        """
        raise NotImplementedError

    @abstractmethod
    def send_bulk_email(self):
        """
        отправка емейлов пачкой в одном запросе
        """
        raise NotImplementedError

    def transform_vars_template(self, template_string):
        """
        заменяет формат для переменных шаблона
        """
        return template_var_pattern.sub(self.template_var_sub_pattern,
                                        template_string)

    def get_subject(self):
        return self.transform_vars_template(self.delivery.email.subject)

    def get_message(self):
        return self.transform_vars_template(self.delivery.email.text)

    def get_message_html(self):
        return self.transform_vars_template(self.delivery.email.html)

    def get_sending_vars(self):
        """
        список переменных в отправляемом письме
        """
        from .validators import validate_email_template_vars
        vars_template = parse_vars_from_template(self.delivery.email.subject)
        vars_template += parse_vars_from_template(self.delivery.email.text)
        vars_template += parse_vars_from_template(self.delivery.email.html)
        validate_text = self.delivery.email.subject + self.delivery.email.text + self.delivery.email.html
        validate_email_template_vars(validate_text)
        return list(set(vars_template))

    def get_vars_for_qs(self):
        """
        словарь переременных для получения значений из qs
        """
        vars_template = self.get_sending_vars()
        kwargs = {}
        args = []
        dict_vars = dict(DELIVERY_ITEM_VALUES_FOR_EMAIL_TEMPLATES)

        for var in vars_template:
            if var != dict_vars.get(var):
                kwargs[var] = F("{}".format(dict_vars.get(var)))
            else:
                args.append(var)

        return {'args': args, 'kwargs': kwargs}


class MailgunBackend(DeliveryEmailBackend):
    def __init__(self, delivery, delivery_account, delivery_item_qs):
        super().__init__(delivery, delivery_account, delivery_item_qs)
        self.template_var_sub_pattern = r'%recipient.\1%'
        self.api_url = "https://api.mailgun.net/v3/{}/messages".format(
            delivery_account.domain_name
        )
        self.limit_batch_count = 500

    def send_mail(self, delivery, delivery_item):
        """
        отправка одного емейла
        """
        send_data = {
            "from": self.delivery.delivery_account.from_email,
            "to": delivery_item.email,
            "subject": delivery.email.subject,
            "text": delivery.email.text,
            "html": delivery.email.html,
        }

        if self.test_mode:
            send_data['o:testmode'] = 'true'

        result = requests.post(
            self.api_url,
            auth=("api", self.api_key),
            data=send_data
        )

        if result.status_code == 200:
            message_info = 'Delivery id:{}. Message from mailgun: "{}"'.format(
                self.delivery.id,
                json.loads(result.content).get('message')
            )
            logger.info(message_info)
            delivery_item.is_sent = True
            delivery_item.save()
            self.delivery.__class__.objects.filter(id=self.delivery.id).update(
                sent=F('sent') + 1)
        else:
            message_error = 'Delivery id:{}. Message from mailgun: "{}"'.format(
                self.delivery.id,
                json.loads(result.content).get('message')
            )
            logger.error(message_error)
            self.delivery.is_active = False
            self.delivery.error = message_error
            self.delivery.save()
            self.delivery.delivery_account.is_active = False
            self.delivery.delivery_account.save()

    def get_recipient_vars(self):
        """
        список словарей, емейл - переменные для шаблона, id
        """
        vars_qs = self.get_vars_for_qs()
        args = list(set(vars_qs.get('args') + ['email', 'id']))
        kwargs = vars_qs.get('kwargs')
        delivery_items_values = self.delivery_item_qs.values(*args, **kwargs)
        return [{item.get('email'): item, 'id': item.pop('id')} for item in delivery_items_values]

    def get_batches(self):
        """
        разбивает всю рассылку на список пакетов согласно лимиту на отправку
        """
        def split_batches(items, limit):
            return [items[i:i + limit] for i in range(0, len(items), limit)]
        # f = lambda A, n=self.limit_batch_count: [A[i:i + n] for i in range(0, len(A), n)]
        return split_batches(self.get_recipient_vars(), self.limit_batch_count)

    @staticmethod
    def parse_batch_data(batch):
        """
        возвращает пакетные данные в виде словаря
        """
        delivery_item_ids = []
        emails = []
        recipient_vars = {}
        for item in batch:
            delivery_item_ids.append(item.pop('id'))
            emails += item.keys()

            recipient_vars.update(item)

        batch_data = {
            'ids': delivery_item_ids,
            'emails': emails,
            'vars_template': json.dumps(recipient_vars)
        }
        return batch_data

    def send_request_to_api(self, ids, emails, vars_template):
        send_data = {
            "from": self.delivery.delivery_account.from_email,
            "to": emails,
            "subject": self.get_subject(),
            "text": self.get_message(),
            "html": self.get_message_html(),
            "recipient-variables": vars_template,
        }

        if self.test_mode:
            send_data['o:testmode'] = 'true'
        result = requests.post(
            self.api_url,
            auth=("api", self.api_key),
            data=send_data
        )
        if result.status_code == 200:
            message_info = 'Delivery id:{}. Message from mailgun: "{}"'.format(
                self.delivery.id,
                json.loads(result.content).get('message')
            )
            logger.info(message_info)
            return len(ids)
        else:
            message_error = 'Delivery id:{}. Message from mailgun: "{}"'.format(
                self.delivery.id,
                json.loads(result.content).get('message')
            )
            logger.error(message_error)
            self.delivery.is_active = False
            self.delivery.error = message_error
            self.delivery.save()
            self.delivery.delivery_account.is_active = False
            self.delivery.delivery_account.save()
            return 0

    def send_bulk_email(self):
        for batch in self.get_batches():
            emails = []
            batch_data = self.parse_batch_data(batch)
            for item in batch:
                emails += item.keys()
                count_sent = self.send_request_to_api(**batch_data)
                self.delivery.__class__.objects.filter(id=self.delivery.id).update(sent=F('sent') + count_sent)
                sending_delivery_items = self.delivery_item_qs.filter(
                    id__in=batch_data.get('ids')
                )
                sending_delivery_items.update(is_sent=True, sent_at=timezone.now())
