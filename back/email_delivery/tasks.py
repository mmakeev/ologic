from django_rq import job


@job('email')
def send_emails(delivery_id):
    from .models import Delivery, DeliveryAccount
    delivery = Delivery.objects.get(id=delivery_id)
    backend_email = DeliveryAccount.SERVICE_BACKENDS.get(
        delivery.delivery_account.service
    )
    backend_email(
        delivery,
        delivery.delivery_account,
        delivery.get_delivery_item_qs()
    ).send_bulk_email()
