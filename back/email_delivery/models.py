from django.conf import settings
from django.contrib import messages
from django.db import models
from django.utils.translation import gettext_lazy as _
from tinymce import HTMLField

from .email_backends import MailgunBackend, TEMPATE_VARS
from .tasks import send_emails
from .validators import validate_email_template_vars


class Delivery(models.Model):
    DELIVERY_NEW = 'new'
    DELIVERY_IN_PROGRESS = 'in_progress'
    DELIVERY_DONE = 'done'
    DELIVERY_CANCELLED = 'cancelled'
    DELIVERY_FAILED = 'failed'
    DELIVERY_STATUS_CHOICES = (
        (DELIVERY_NEW, _('new')),
        (DELIVERY_IN_PROGRESS, _('in progress')),
        (DELIVERY_DONE, _('done')),
        (DELIVERY_CANCELLED, _('cancelled')),
        (DELIVERY_FAILED, _('failed')),
    )
    name = models.CharField(max_length=255)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                                   on_delete=models.SET_NULL)
    initial_amount = models.IntegerField(default=0)
    status = models.CharField(max_length=10, choices=DELIVERY_STATUS_CHOICES,
                              default=DELIVERY_NEW)
    error = models.TextField(blank=True)
    sent = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True,
                                blank=True)
    email = models.ForeignKey('Email', on_delete=models.SET_NULL, null=True)
    tag_slug = models.SlugField(max_length=255, null=True, blank=True)
    is_active = models.BooleanField(default=False)
    delivery_account = models.ForeignKey('DeliveryAccount', null=True,
                                         on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'рассылка'
        verbose_name_plural = 'рассылки'

    def __str__(self):
        return self.name

    def send_butch_emails(self, request=None):
        if self.is_active and self.delivery_account.is_active:
            send_emails.delay(delivery_id=self.id)
        else:
            if request:
                messages.error(request, 'Delivery or Delivery Account is not '
                                        'active')
            else:
                raise Exception('Delivery or Delivery Account is not active')

    def get_delivery_item_qs(self):
        return self.deliveryitem_set.filter(is_sent=False)


class DeliveryItem(models.Model):
    delivery = models.ForeignKey(Delivery, on_delete=models.SET_NULL, null=True)
    email = models.EmailField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True,
                             on_delete=models.SET_NULL)
    is_sent = models.BooleanField(default=False, help_text=_('sent in queeu'))
    sent_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name_plural = 'письма в очереди'

    def __str__(self):
        return '{}: {}'.format(self.email, self.delivery.name)


class DeliveryAccount(models.Model):
    SERVICE_CHOICES = (
        ('mailgun', _('MailGun')),
    )

    SERVICE_BACKENDS = {
        'mailgun': MailgunBackend,
    }

    name = models.CharField(max_length=255)
    service = models.CharField(max_length=100, choices=SERVICE_CHOICES)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                             on_delete=models.SET_NULL)
    api_key = models.TextField()
    domain_name = models.CharField(max_length=50)
    from_email = models.CharField(help_text=_('Excited User <user@example.com>'),
                                  max_length=100)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    test_mode = models.BooleanField(default=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Аккаунт для рассылки'
        verbose_name_plural = 'Аккаунты для рассылки'

    def __str__(self):
        return self.name


class Email(models.Model):
    TYPE_REGULAR_EMAIL = 'regular_email'
    TYPE_CHAIN = 'chain'

    CHOICES_TYPES = (
        (TYPE_REGULAR_EMAIL, _('regular email')),
        (TYPE_CHAIN, _('chain')),
    )

    subject = models.CharField(max_length=255,
                               validators=[validate_email_template_vars])
    text = models.TextField(validators=[validate_email_template_vars])
    html = HTMLField(validators=[validate_email_template_vars])
    slug = models.SlugField(unique=True)
    type_email = models.CharField(max_length=20, choices=CHOICES_TYPES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['subject']
        verbose_name = 'шаблон письма'
        verbose_name_plural = 'шаблоы писем'

    def __str__(self):
        return self.subject

    @staticmethod
    def template_vars():
        return ['{{ %s }}' % v for v in TEMPATE_VARS]
