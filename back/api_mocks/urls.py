from django.conf.urls import url

from . import views

app_name = 'api_mocks'

urlpatterns = [
    url(r"^settings/email/$", view=views.mock_settings_email, name='settings_email'),
]
