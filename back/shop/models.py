import os
import uuid

from django.db import models
from djmoney.models.fields import MoneyField
from django.core.validators import MaxValueValidator, MinValueValidator
from django.urls import reverse


class Goods(models.Model):
    LETTER = 'letter'
    GOODS_TYPES = (
        (LETTER, 'письмо'),
    )
    name = models.CharField('название', max_length=200)
    slug = models.SlugField(max_length=255)
    goods_type = models.CharField('тип товара', max_length=20, choices=GOODS_TYPES)
    description = models.TextField('описание')
    price = MoneyField('цена', max_digits=14, decimal_places=2,
                       default_currency='RUB')
    partner_reward = models.IntegerField('парнерское вознаграждение', default=0,
                                         validators=[MinValueValidator(0), MaxValueValidator(100)])
    is_published = models.BooleanField('опубликован', default=False)

    class Meta:
        ordering = ['name']
        verbose_name = 'товар'
        verbose_name_plural = 'товары'

    def __str__(self):
        return '{}:{}'.format(self.id, self.name)

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug,
            'id': self.pk
        }
        return reverse('shop:goods-detail', kwargs=kwargs)

    @property
    def absolute_url(self):
        return self.get_absolute_url()

    class Context:
        fields = ['__all__']
        extra_attrs = ['absolute_url']


def get_file_path(instance, filename):
    _, ext = os.path.splitext(filename)
    filename = '{}.{}'.format(uuid.uuid4(), ext)
    return os.path.join('goods_iamges/', filename)


class GoodsImage(models.Model):
    photo = models.ImageField('Изображение товара', upload_to=get_file_path)
    goods = models.ForeignKey(Goods, on_delete=models.CASCADE)

    class Meta:
        ordering = ['id']
        verbose_name = 'Изображение товара'
        verbose_name_plural = 'Изобрважения товаров'
