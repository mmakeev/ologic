from django.urls import path
from .views import GoodsListView, GoodsDetailView, GoodsPayView

app_name = 'shop'


urlpatterns = [
    path('', view=GoodsListView.as_view(), name='goods-list'),
    path('<slug:slug>-<int:id>/', GoodsDetailView.as_view(), name='goods-detail'),
    path('<slug:slug>-<int:id>/pay/', GoodsPayView.as_view(), name='goods-pay'),
]
