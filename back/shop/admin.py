from django.contrib import admin
from .models import Goods, GoodsImage
from project.admin import custom_admin_site
from boolean_switch.admin import AdminBooleanMixin


class GoodsPhotoInline(admin.TabularInline):
    model = GoodsImage


@admin.register(Goods, site=custom_admin_site)
class GoodsAdmin(AdminBooleanMixin, admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'goods_type', 'price', 'is_published', 'partner_reward', )
    list_filter = ('is_published', 'goods_type')
    prepopulated_fields = {"slug": ("name",)}
    inlines = [GoodsPhotoInline]


