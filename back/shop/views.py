from django.views.generic import ListView, DeleteView, TemplateView
from .models import Goods
from django.http import Http404


class GoodsListView(ListView):
    template_name = 'shop/goods-list.pug'
    queryset = Goods.objects.filter(is_published=True)


class GoodsDetailView(DeleteView):
    template_name = 'shop/goods-detail.pug'
    queryset = Goods.objects.filter(is_published=True)

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(**self.kwargs)
        return qs

class GoodsPayView(TemplateView):
    template_name = 'shop/goods-pay.pug'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        try:
            goods = Goods.objects.get(**self.kwargs)
            ctx['goods'] = goods
        except Goods.DoesNotExist:
            raise Http404
        return ctx
