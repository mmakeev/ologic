CURRENCIES = ('RUB', 'USD',)


# используется в django-money для админки
DEFAULT_CURRENCY = 'RUB'
CURRENCY_CHOICES = [
    ('RUB', 'р.'),
    ('USD', 'USD $'),
]

CURRENCY_DECIMAL_PLACES = 6  # при таком значении цисла в админке выглядят прилично
