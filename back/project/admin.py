import logging
from django.contrib.admin import AdminSite
from django.contrib.admin.models import LogEntry
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.models import Group
from constance.admin import Config, ConstanceAdmin
from logentry_admin.admin import LogEntryAdmin
from django_cron.admin import CronJobLog, CronJobLogAdmin
from sorl.thumbnail.admin import AdminImageMixin
from django.utils.translation import gettext_lazy as _
from django.conf.urls import url
from main.models import (
    User,
)
from import_export import resources
from import_export.fields import Field
from import_export.admin import ImportExportModelAdmin
from main.views.admin import change_active_school_view
from boolean_switch.admin import AdminBooleanMixin


logger = logging.getLogger(__name__)


class CustomizedAdminSite(AdminSite):
    site_header = 'Online-Logic Site'
    site_title = 'Online-Logic'
    index_title = 'Admin'
    empty_value_display = '¯\_(ツ)_/¯'

    def index(self, request, extra_context=None):
        """
        Можно просунуть дополнительный контент для главной страницы админки
        """
        if extra_context is None:
            extra_context = {}

        extra_context.update({
            # 'stats': stats,
        })

        return super(CustomizedAdminSite, self).index(request, extra_context)

    def get_urls(self):
        """
        Кастомные немодельные админские страницы нужно добавлять вот таким образом
        """
        urlpatterns = super().get_urls()
        urlpatterns += [
            url(r'^crm/change_active_school/$',
                self.admin_view(change_active_school_view),
                name='change_active_school'
            ),
        ]
        return urlpatterns


custom_admin_site = CustomizedAdminSite()


class UserResource(resources.ModelResource):
    full_name = Field()

    class Meta:
        model = User
        fields = ('id', 'full_name', 'email', 'phone')
        export_order = ('id', 'full_name', 'email', 'phone')

    def dehydrate_full_name(self, user):
        return user.get_full_name()


class CustomUserAdmin(AdminImageMixin, AdminBooleanMixin, UserAdmin,
                      ImportExportModelAdmin):
    # change_form_template = 'admin/user_change_view.html'

    model = User
    list_display = ['_username', 'first_name', 'last_name', 'is_staff',
                    'is_superuser', 'is_active']
    readonly_fields = ['last_login', 'date_joined']
    search_fields = ('username', 'first_name', 'last_name', 'email', '=id')
    list_per_page = 100
    ordering = ['-id']
    resource_class = UserResource

    def _username(self, obj):
        if isinstance(obj.username, str):
            return obj.username.lower()
        return obj.username

    fieldsets = (
        (None, {'fields': ('username', 'password',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'is_email_verified')}),
        (_('Other data'), {'fields': (
            'gender',
            'birthday',
            'phone',
            'is_phone_verified',
            'avatar',
            'about',)}),
        (_('Permissions'), {'fields': (
            'is_active',
            'is_staff',
            'is_superuser',
            'groups')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined', 'password_changed_at')}),
    )
    actions_on_top = False
    actions_on_bottom = False
    actions = None

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """
        Кастомный контекст для страницы редактирования
        """
        extra_context = extra_context or {}
        user = User.objects.get(id=object_id)
        # extra_context['subscription_link'] = reverse(
        #     'primalbase:manage_subscription', kwargs={'subscription_code': user.subscription_code}
        # )
        return super().change_view(request, object_id, form_url=form_url, extra_context=extra_context)

    def has_import_permission(self, request):
        return False


custom_admin_site.register(User, CustomUserAdmin)
custom_admin_site.register(Group, GroupAdmin)
custom_admin_site.register(LogEntry, LogEntryAdmin)
custom_admin_site.register([Config], ConstanceAdmin)
custom_admin_site.register(CronJobLog, CronJobLogAdmin)
