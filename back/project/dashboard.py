from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.utils.safestring import mark_safe

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name
from admin_tools.menu.models import Bookmark
from admin_tools.menu import items, Menu


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for src.
    """
    # template = 'admin/dashboard.html'

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        # append a link list module for "quick links"

        bookmark_links = []
        for b in Bookmark.objects.filter(user=context['request'].user):
            bookmark_links.append([mark_safe(b.title), b.url])

        self.children.append(modules.LinkList(
            _(u'Закладки'),
            # layout='inline',
            # draggable=False,
            deletable=False,
            # collapsible=False,
            children=bookmark_links
        ))

        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='inline',
            # draggable=False,
            deletable=False,
            # collapsible=False,
            children=[
                [_(u'Написать разработчику'), 'tg://resolve?domain=mmakeev'],
                [_(u'Очистить кэш'), '/clear_cache'],
                [_(u'Django rq'), reverse('rq_home')],
                [_('Change password'), reverse('%s:password_change' % site_name)],
                [_('Log out'), reverse('%s:logout' % site_name)],
            ]
        ))

        self.children.append(modules.ModelList(
            'Блоги',
            models=('blogs.*',)
        ))

        self.children.append(modules.ModelList(
            'Магзин',
            models=('shop.*',)
        ))

        self.children.append(modules.ModelList(
            'Страны и города',
            models=('main.models.cities_light.*',)
        ))

        self.children.append(modules.ModelList(
            'Города и регионы',
            models=('locations.models.*',),
        ))

        self.children.append(modules.ModelList(
            'CRM',
            models=(
                'main.models.user.User', 'main.models.crm.Person',
                'main.models.crm.List', 'main.models.school.School',
            ),
        ))

        self.children.append(modules.ModelList(
            'Вебинары',
            models=(
                'main.models.webinar.Webinar',
            ),
        ))

        self.children.append(modules.ModelList(
            'Контент',
            models=('main.models.mediafile.*', 'taggit.*',
                    'main.models.event.Event', 'main.models.contentpage.ContentPage')
        ))

        self.children.append(modules.ModelList(
            'Рассылки',
            models=('email_delivery.models.*',)
        ))

        # append an app list module for "Administration"
        self.children.append(modules.ModelList(
            _('Administration'),
            models=('django.contrib.*', 'site_settings.models.*', 'constance.*',
                    'django_cron.*',),
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 5))


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for src.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)


class CustomMenu(Menu):
    """
    Custom Menu for src admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.Bookmarks(),
            items.AppList(
                _(u'Контент'),
                models=('catalog.models.*', 'subscription.models.*')
            ),
            # items.AppList(
            #     _('Рассылки'),
            #     models=('subscription.models.*',)
            # ),
            items.AppList(
                _('Administration'),
                models=('django.contrib.*',)
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
