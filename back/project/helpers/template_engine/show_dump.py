import pprint
import inspect
import simplejson
from django.conf import settings
from datetime import datetime, date, timedelta
from pytz.tzinfo import BaseTzInfo
from itertools import chain

from django.http import JsonResponse
from django.utils.functional import SimpleLazyObject
from django.db.models.query import QuerySet
from django.db.models import Model
# from dictator import DictatorContent
from django.core.handlers.wsgi import WSGIRequest
from djmoney.money import Money, Currency
from django.template.backends.django import DjangoTemplates, reraise
from django.template import TemplateDoesNotExist
from django.template.context import make_context


MAX_DEPTH = 5  # это чтобы не охуеть от вложенности
TYPES_TO_DUMP = (date, datetime, timedelta, Money, Currency, BaseTzInfo)


def should_dump(request):
    return request.GET.get('dump') is not None


class DumpJsonEncoder(simplejson.JSONEncoder):
    def __init__(self, *args, **kwargs):
        try:
            self.silent = kwargs.pop('silent')
        except KeyError:
            self.silent = False

        super(DumpJsonEncoder, self).__init__(*args, **kwargs)

    def default(self, obj):
        return self.parse_obj(obj)

    def parse_obj(self, obj, depth=1):
        if depth >= MAX_DEPTH:
            return {
                '@@error': 'MAX_DEPTH exceeded',
                '@@content': pprint.pformat(obj)
            }

        # этот объект умеет прикидываться строкой и все ломать
        if isinstance(obj, SimpleLazyObject):
            # hack чтобы разбудить LazyObject
            obj._setup()
            return self.parse_obj(obj._wrapped)

        # сраный диктатор унаследован от str, поэтому его надо парсить пораньше
        # if isinstance(obj, DictatorContent):
        #     return {
        #         '@@type': str(type(obj)),
        #         '@@content': {
        #             'html': obj.html,
        #             'json': obj.json,
        #             'raw_indent': obj.raw_indent
        #         },
        #     }

        if isinstance(obj, (datetime, date)):
            return {
                '@@type': str(type(obj)),
                '@@content': pprint.pformat(obj),
            }

        # тут большинство должно уйти на выход до наступления MAX_DEPTH
        if isinstance(obj, (int, float, str, bool, list, dict)):
            return obj

        # всякие специальный классы, которые нам нужны
        if isinstance(obj, TYPES_TO_DUMP):
            return self.obj_to_dict(obj, depth=depth)

        # наш правильный объект с Context
        if hasattr(obj, 'Context') and isinstance(obj, Model):
            return self.context_to_dict(obj, depth)

        # Model, может быть с Context или без
        if isinstance(obj, Model):
            dic = self.obj_to_dict(obj, depth=depth)
            dic['@@type'] = 'Model: %s' % str(type(obj))
            return dic

        # QuerySet содержит модели, которые так же могут быть с Context
        if isinstance(obj, QuerySet):
            query_list = []
            for model_instance in obj:
                # QuerySet это набор из Model, поэтому тут рекурсия
                query_list.append(self.default(model_instance))
            return {
                # QuerySet это iterable но не list, а мы договорились не вводить в заблуждение о настоящем типе объекта.
                # Фронт должен знать как работать с QuerySet и понимать отличие его от обычного list
                '@@type': 'QuerySet: %s' % obj.model,
                '@@content': query_list
            }

        # объект Request
        if isinstance(obj, WSGIRequest):
            request = {
                '@@type': str(type(obj)),
                'scheme': obj.scheme,
                'path': obj.path,
                'method': obj.method,
                'GET': obj.GET,
                'POST': obj.POST,
                'COOKIES': obj.COOKIES
            }

            return request

        # неведомая ёбаная хуйня
        # (ManyRelated, partial, validators и прочее джанговское и не очень)
        return pprint.pformat(obj)

    def obj_to_dict(self, obj, depth, exclude=None):
        # все остальные объекты
        dic = {}

        try:
            for attr, value in inspect.getmembers(obj):
                if exclude and str(attr) in exclude:
                    continue

                if not (attr.startswith('_') or attr == 'Meta' or inspect.isroutine(value)):
                    value_to_dump = self.parse_obj(value, depth=depth+1)
                    dic[str(attr)] = value_to_dump

        except Exception:
            dic['@@dump error'] = '@@ Unable to inspect this object @@'
            pass

        return dic

    def context_to_dict(self, instance, depth):
        """
        Делает dict из объекта модели согласно model.Context
        """
        opts = instance._meta
        data = {}

        fields = getattr(instance.Context, 'fields', [])
        extra_attrs = getattr(instance.Context, 'extra_attrs', [])
        exclude = getattr(instance.Context, 'exclude', [])

        if '__all__' in fields:
            for f in chain(opts.concrete_fields, opts.private_fields):
                if exclude and f.name in exclude or f.name.endswith('_ru') or f.name.endswith('_en'):
                    continue

                data[f.name] = self.parse_obj(getattr(instance, f.name), depth=depth+1)
        else:
            for fname in fields:
                if exclude and fname in exclude:
                    continue
                f = getattr(instance, fname)
                data[fname] = self.parse_obj(f, depth=depth+1)

        if extra_attrs:
            for attr_name in extra_attrs:
                try:
                    attr = getattr(instance, attr_name)
                    if callable(attr):
                        attr_name = '%s()' % attr_name
                        attr_value = attr()
                    else:
                        attr_value = attr

                    data[attr_name] = self.parse_obj(attr_value, depth=depth+1)
                except Exception as e:
                    if self.silent:
                        data[attr_name] = '@@exception: %s' % str(e)
                    else:
                        raise e

        return data


class DumpJsonResponseMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if settings.DEBUG:
            if (response.status_code == 200 and should_dump(request)):
                response['Content-Type'] = 'application/javascript; charset=utf-8'

        return response


class PugTemplate:

    def __init__(self, template, backend):
        self.template = template
        self.backend = backend

    @property
    def origin(self):
        return self.template.origin

    def render(self, context=None, request=None):
        context = make_context(context, request, autoescape=self.backend.engine.autoescape)

        if should_dump(request):
            data_for_template = self.get_data_for_template(context, request)
            return self.get_dump(data_for_template).strip('"')

        try:
            return self.template.render(context)
        except TemplateDoesNotExist as exc:
            reraise(exc, self.backend)

    def get_data_for_template(self, context=None, request=None):
        context = context.flatten()
        context['request'] = request
        return context

    def get_dump(self, data_for_template):
        return simplejson.dumps(data_for_template, cls=DumpJsonEncoder, skipkeys=True, sort_keys=True)


class PugTemplates(DjangoTemplates):

    app_dirname = 'templates'

    def from_string(self, template_code):
        return PugTemplate(self.engine.from_string(template_code), self)

    def get_template(self, template_name):
        try:
            return PugTemplate(self.engine.get_template(template_name), self)
        except TemplateDoesNotExist as exc:
            reraise(exc, self)

