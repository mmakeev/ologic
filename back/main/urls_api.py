from django.conf.urls import url
from .views.endpoints import (
    get_cities_by_country,
    RegistrationView
)

app_name = 'api'


urlpatterns = [
    url(r'^api/cities/get_by_country/$', view=get_cities_by_country, name='get_cities_by_country'),
    url(r'^api/rest-auth/registration/$', view=RegistrationView.as_view(), name='user_registration'),
]
