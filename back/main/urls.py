from django.conf import settings
from django.conf.urls import url
from .views.pages import (
    HomepageView, RegistrationPage, MockupView
)


app_name = 'ologic'


urlpatterns = [
    url(r'^$', view=HomepageView.as_view(), name='home'),
    url(r'^registration/$', view=RegistrationPage.as_view(), name='registration'),
]

if settings.DEBUG:
    # моковые страницы
    urlpatterns += [
        url(r'^expert-profile-mockup/$', view=MockupView.as_view(template_name='pages/index/expert-profile.pug')),
        url(r'^login-page/$', view=MockupView.as_view(template_name='pages/index/login-page.pug')),
        url(r'^autowebinar/$', view=MockupView.as_view(template_name='pages/index/autowebinar.pug')),
        url(r'^autowebinar-age/$', view=MockupView.as_view(template_name='pages/index/autowebinar-age.pug')),
        url(r'^content-page/$', view=MockupView.as_view(template_name='pages/index/content-page.pug')),
        # url(r'^context-page-mockup/$', view=MockupView.as_view(template_name='pages/index/context_page.pug')),
    ]
