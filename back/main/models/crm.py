from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.text import Truncator
from admin_comments.models import Comment
from sorl.thumbnail import ImageField


class List(models.Model):
    """
    Списки принадлежат онлайн школам, каждая школа может смотреть только свои
    школы.
    """
    name = models.CharField(max_length=300)
    slug = models.SlugField()
    school = models.ForeignKey('main.School', on_delete=models.CASCADE)
    is_default = models.BooleanField(default=False, help_text='список куда падают пользователи при создании, '
                                                              'создается автоматически при создании школы')

    class Meta:
        verbose_name = "Список"
        verbose_name_plural = "Списки"
        unique_together = ('slug', 'school')

    def __str__(self):
        return self.name


class Person(models.Model):
    school = models.ForeignKey('main.School', on_delete=models.CASCADE, null=True,
                               verbose_name='школа')
    list = models.ForeignKey(List, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True,
                             editable=False, on_delete=models.CASCADE,
                             verbose_name='профиль на сайте',
                             help_text='связывается с пользователем в момент подтверждения email')
    social = models.URLField(blank=True, null=True, default='',
                             help_text='url на профиль в соцсети')
    is_subscribed = models.BooleanField('получает рассылки', default=False, help_text='получает рассылки или нет')
    source = models.CharField('источник', editable=False, max_length=300, null=True, blank=True,
                              help_text='откуда пришел пользователь, подставляется автоматически')
    created_at = models.DateTimeField('добавлен', auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    comments = GenericRelation(Comment)

    class Meta:
        verbose_name = "Ученик"
        verbose_name_plural = "Ученики"
        unique_together = ('user', 'school')

    def get_full_name(self):
        full_name = '%s %s' % (self.user.first_name, self.user.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.user.first_name

    def get_last_comment(self):
        """
        Последний комментарий для вывода в админке
        """
        last_comment = self.comments.last()
        if last_comment:
            return Truncator(last_comment.comment).chars(50)
    get_last_comment.short_description = 'Последний комментарий'

    def __str__(self):
        return self.user.email if self.user else ''
