from django.db import models
from django.conf import settings
from sorl.thumbnail import ImageField


class School(models.Model):
    name = models.CharField(max_length=300)
    slug = models.SlugField(unique=True)
    description = models.TextField(blank=True, null=True, default='')
    image = ImageField(null=True, blank=True, upload_to='schools')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                             on_delete=models.SET_NULL, help_text='владелец школы')
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Онлайн школа"
        verbose_name_plural = "Онлайн школы"

    def __str__(self):
        return self.name
