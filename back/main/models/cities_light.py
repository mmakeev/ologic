from cities_light.abstract_models import (AbstractCity, AbstractRegion,
                                          AbstractCountry)
from cities_light.receivers import connect_default_signals
from django.db import models


class Country(AbstractCountry):
    order_number = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ["order_number"]
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'

connect_default_signals(Country)


class Region(AbstractRegion):
    order_number = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ["order_number"]
        verbose_name_plural = 'Регион'
        verbose_name = 'Регионы'


connect_default_signals(Region)


class City(AbstractCity):
    order_number = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ["order_number"]
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


connect_default_signals(City)
