import csv
from datetime import datetime, timedelta
from django.db import models
from django.conf import settings
from sorl.thumbnail import ImageField
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator


def datetime_field_validator(value):
    """
    Валидатор датывремени для файла чатлога
    """
    try:
        datetime.strptime(value, '%d.%m.%Y %H:%M:%S')
    except ValueError:
        raise ValidationError('неверный формат даты, правильный формат: %d.%m.%Y %H:%M:%S')


class Webinar(models.Model):
    name = models.CharField('название', max_length=300)
    slug = models.SlugField(unique=True, help_text='используется для красивых url и как источник в CRM')
    description = models.TextField('описание', blank=True, null=True, default='')
    image = ImageField('картинка', null=True, blank=True, upload_to='schools')
    school = models.ForeignKey('main.School', on_delete=models.CASCADE,
                               verbose_name='онлайн школа')
    # list = models.ForeignKey('main.List', verbose_name='список', null=True, on_delete=models.SET_NULL,
    #                          help_text='куда складывать зарегистрированных на вебинар')
    host = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             verbose_name='ведущий', help_text='кто проводит вебинар')
    time_start = models.TimeField('время начала')
    is_published = models.BooleanField('опубликован', default=False)
    video_url = models.URLField('видео', help_text='ссылка на youtube или платформу для живых вебинаров')
    video_seconds = models.IntegerField('продолжительность видео', null=True, blank=True,
                                        help_text='в секундах')
    moderator_name = models.CharField('имя модератора в чате', max_length=50)
    chatlog_dt = models.CharField('Время начала вебинара в файле лога', max_length=30,
                                  blank=True, null=True, validators=[datetime_field_validator],
                                  help_text='необходимо чтобы правильно привязать время сообщений к вебинару')
    chatlog_file = models.FileField('CSV лог чата', blank=True, null=True, upload_to='chatlogs',
                                    validators=[FileExtensionValidator(allowed_extensions=['csv'])],
                                    help_text='при загрзке старый чат будет удален')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = "Вебинар"
        verbose_name_plural = "Вебинары"
        ordering = ['-created_at']

    def __str__(self):
        return self.name


def time_field_validator(value):
    """
    Валидатор времени для сообщений в чате вебинара
    """
    if value.startswith('-'):
        # время может быть с минусом, не учитываем его при валидации
        value = value[1:]

    try:
        datetime.strptime(value, '%H:%M:%S')
    except ValueError:
        raise ValidationError('неверный формат даты, правильный формат: 00:00:00')


class WebinarChatMessage(models.Model):
    """
    Сообщения виртуальных пользователей, заранее загруженные в систему
    """
    webinar = models.ForeignKey(Webinar, on_delete=models.CASCADE)
    time = models.CharField('время', validators=[time_field_validator], max_length=9,
                            help_text='через сколько секунд показывать сообщение')
    name = models.CharField('имя', max_length=50)
    message = models.TextField('сообщение')

    class Meta:
        ordering = ['time']

    def __str__(self):
        return self.message


class WebinarLink(models.Model):
    """
    Рекламные ссылки показываются в отдлельном окне.
    Ссылки на инстаграм ведущего, на курс, на страницу ВК
    """
    webinar = models.ForeignKey(Webinar, on_delete=models.CASCADE)
    time = models.CharField('время', validators=[time_field_validator], max_length=9,
                            help_text='через сколько секунд показывать сообщение')
    name = models.CharField('название ссылки', max_length=1024)
    url = models.URLField('ссылка')

    class Meta:
        ordering = ['time']

    def __str__(self):
        return self.url


@receiver(post_save, sender=Webinar)
def create_default_lists(sender, instance, created, **kwargs):
    """
    Парсиим и сохраняем чатлог
    """
    if not instance.chatlog_file or not instance.chatlog_dt:
        return

    # тут надо распарсить время из первого столбца и корректно сохранить в базу
    # время в файле будет с датой 07.02.2019 21:08:14
    reader = csv.reader(instance.chatlog_file.read().decode('utf-8').splitlines())
    webinar_start = datetime.strptime(instance.chatlog_dt, '%d.%m.%Y %H:%M:%S')

    # удаляем старый чат
    WebinarChatMessage.objects.filter(webinar=instance).delete()
    for line in reader:
        # пытаемся распарсить время из даты
        try:
            t = datetime.strptime(line[0], '%d.%m.%Y %H:%M:%S')
        except ValueError:
            # какая-то левая строка(заголовок или комментарий), пропускаем
            continue

        name = line[1].strip()
        message = line[2].strip()

        if not name or not message:
            continue

        # считаем относительное время, без учета даты
        relevant_dt = int(t.timestamp() - webinar_start.timestamp())
        if relevant_dt < 0:
            formated_time = '-0%s' % str(timedelta(seconds=int(abs(relevant_dt))))
        else:
            formated_time = '0%s' % str(timedelta(seconds=int(relevant_dt)))

        WebinarChatMessage.objects.create(
            webinar=instance,
            time=formated_time,
            name=name,
            message=message
        )

    # не храним распарсенные данные
    instance.chatlog_file = None
    instance.chatlog_dt = None
    instance.save()
