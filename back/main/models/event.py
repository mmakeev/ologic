from datetime import datetime
from django.db import models
from django.db.models import CASCADE
from sorl.thumbnail import ImageField


class EventsQueryset(models.QuerySet):
    def upcoming(self):
        today = datetime.today()
        return self.filter(date__date__gte=today).order_by('date')

    def passed(self):
        today = datetime.today()
        return self.filter(date__date__lt=today).order_by('-date')


class EventsManager(models.Manager):
    def get_queryset(self):
        return EventsQueryset(self.model, using=self._db).select_related('school')

    def upcoming(self):
        return self.get_queryset().upcoming()

    def passed(self):
        return self.get_queryset().passed()


class Event(models.Model):
    school = models.ForeignKey('main.School', on_delete=CASCADE, verbose_name='школа')
    slug = models.SlugField(unique=True, help_text='используется для красивых url и как источник в CRM')
    name = models.CharField('название', max_length=100)
    description = models.TextField('описание', blank=True, default='')
    date = models.DateTimeField('дата')
    duration = models.DurationField('продолжительность', null=True, blank=True)
    external_link = models.URLField('ссылка на событие', null=True, blank=True,
                                    help_text='на вебинар/страницу ВК/внутренню страницу')
    image = ImageField('картинка', upload_to='events', null=True)
    is_published = models.BooleanField('опубликовано', default=True)

    objects = EventsManager()

    class Meta:
        ordering = ['-date']
        verbose_name = 'событие'
        verbose_name_plural = 'события'

    class Context:
        fields = ['__all__']

    def __str__(self):
        return self.name
