from django.db import models
from django.conf import settings
from tinymce import HTMLField
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey


class ContentPage(MPTTModel):
    title = models.CharField('заголовок', max_length=300)
    slug = models.SlugField(unique=True)
    html = HTMLField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                               on_delete=models.SET_NULL, verbose_name='автор')
    exclude_from_sitemap = models.BooleanField('исключить из sitemap', default=False)
    auth_only = models.BooleanField('только для авторизованных', default=False)
    is_published = models.BooleanField('опубликовать', default=False)
    created_at = models.DateTimeField('дата создания', auto_now_add=True)
    updated_at = models.DateTimeField('последнее обновление', auto_now=True)

    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                            related_name='children', verbose_name='родитель')

    class MPTTMeta:
        order_insertion_by = ['title']

    class Meta:
        verbose_name = "Страница"
        verbose_name_plural = "Страницы"

    class Context:
        fields = ['__all__']
        extra_attrs = ['absolute_url']

    def get_absolute_url(self):
        return reverse('ologic:pages', kwargs={'slug': self.slug})

    @property
    def absolute_url(self):
        return self.get_absolute_url()

    def __str__(self):
        return self.title
