import uuid
import base58
import hashlib
import logging
from pytz import utc
from datetime import datetime
from binascii import unhexlify
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.core import signing
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from .cities_light import City
from sorl.thumbnail import ImageField


logger = logging.getLogger(__name__)


def uuid4_hex():
    return uuid.uuid4().hex


class UserModelManager(UserManager):
    """
    Регистронезависимый поиск и валидация юзера (username и email)
    """

    def filter(self, **kwargs):
        if 'username' in kwargs:
            kwargs['username__iexact'] = kwargs['username']
            del kwargs['username']
        if 'email' in kwargs:
            kwargs['email__iexact'] = kwargs['email']
            del kwargs['email']
        return super(UserModelManager, self).filter(**kwargs)

    def get(self, **kwargs):
        if 'username' in kwargs:
            kwargs['username__iexact'] = kwargs['username']
            del kwargs['username']
        if 'email' in kwargs:
            kwargs['email__iexact'] = kwargs['email']
            del kwargs['email']
        return super(UserModelManager, self).get(**kwargs)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        username = email
        return self._create_user(username, email, password, **extra_fields)

class User(AbstractUser):
    class GENDER:
        MALE = 'male'
        FEMALE = 'female'

    GENDER_CHOICES = (
        (GENDER.MALE, 'male'),
        (GENDER.FEMALE, 'female'),
    )

    username_validator = ASCIIUsernameValidator()
    email_validator = EmailValidator()

    username = models.CharField(_('username'), max_length=150, blank=True, null=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    email = models.EmailField(_('email address'), unique=True)
    is_email_verified = models.BooleanField('email подтвержден', default=False)
    phone = models.CharField(max_length=30, null=True, blank=True, default='')
    is_phone_verified = models.BooleanField(default=False)
    password_changed_at = models.DateTimeField(null=True, blank=True)
    about = models.TextField(blank=True, default='')
    avatar = ImageField(null=True, blank=True, upload_to='avatars')
    gender = models.CharField(max_length=10, null=True, blank=True, choices=GENDER_CHOICES)
    birthday = models.DateField(null=True, blank=True)
    city = models.ForeignKey(City, null=True, on_delete=models.SET_NULL)
    # schools = models.ManyToManyField('main.School', related_name='users')

    # Пока нет, обходимся валютой по умолчанию.
    # currency = CurrencyField(choices=settings.CURRENCY_CHOICES, default=settings.DEFAULT_CURRENCY)

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserModelManager()

    def get_signed_hash(self):
        signed_str = '%d_%s' % (self.id, hashlib.md5(settings.SECRET_KEY.encode('utf-8')).hexdigest())
        return hashlib.sha256(signed_str.encode('utf-8')).hexdigest()[:16]

    @property
    def subscription_code(self):
        """
        Код для управлением подписками
        """
        signed_hash = self.get_signed_hash()
        str_to_encode = '%d_%s' % (self.id, signed_hash)
        return base58.b58encode(str_to_encode.encode('utf-8'))

    def set_password(self, *args, **kwargs):
        """
        Фиксация момента смены пароля.
        """
        super().set_password(*args, **kwargs)
        self.password_changed_at = datetime.now().astimezone(utc)
        # убиваем старые сессии
        # AuthSession.objects.filter(user=self).update(is_active=False)

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        # permissions = (
        #     ("can_verify_user", "Can set verified status for user"),
        # )


class EmailConfirmationHMAC:
    """
    Код подтверждения email и его проверка
    """

    def __init__(self, user):
        self.user = user

    @property
    def key(self):
        return signing.dumps(obj=self.user.pk, salt=settings.EMAIL_CONFIRMATION_SECRET)

    @classmethod
    def from_key(cls, key):
        try:
            max_age = (
                60 * 60 * 24 * settings.EMAIL_CONFIRMATION_EXPIRE_DAYS)
            pk = signing.loads(key, max_age=max_age, salt=settings.EMAIL_CONFIRMATION_SECRET)
            ret = EmailConfirmationHMAC(User.objects.get(pk=pk, is_active=True))
        except (signing.SignatureExpired, signing.BadSignature, User.DoesNotExist):
            ret = None
        return ret

    def confirm(self):
        if not self.user.is_email_verified:
            self.user.is_email_verified = True
            self.user.save()
            return True
        return False


def key_validator(value):
    return hex_validator()(value)


def hex_validator(length=0):
    """
    Returns a function to be used as a model validator for a hex-encoded CharField.
    """
    def _validator(value):
        try:
            if isinstance(value, str):
                value = value.encode()

            unhexlify(value)
        except Exception:
            raise ValidationError('{0} is not valid hex-encoded data.'.format(value))

        if (length > 0) and (len(value) != length * 2):
            raise ValidationError('{0} does not represent exactly {1} bytes.'.format(value, length))

    return _validator
