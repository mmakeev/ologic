from django.http import HttpResponseForbidden
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from main.models import School


@csrf_exempt
@require_http_methods(['POST'])
def change_active_school_view(request):
    """
    Меняет активную школу в CRM
    """
    if not request.user.is_staff:
        return HttpResponseForbidden()

    school_id = int(request.POST['school_id'])
    school = get_object_or_404(School, pk=school_id, owner=request.user)
    request.session['active_school_id'] = school.id

    return JsonResponse({'success': True, 'school': school.id})
