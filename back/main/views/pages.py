from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from main.mixins import (
    MenuContextMixin,
    login_page_view
)
from project.helpers.cache_control_view import CacheMixin
from main.models import (
    ContentPage
)
from cities_light.loading import get_cities_models


Country, Region, City = get_cities_models()


class MockupView(MenuContextMixin, CacheMixin, TemplateView):
    pass


class HomepageView(MenuContextMixin, CacheMixin, TemplateView):
    template_name = 'pages/index/index.pug'

    cache_max_age = 60

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['test'] = 123
        return context


class RegistrationPage(MenuContextMixin, CacheMixin, TemplateView):
    template_name = 'pages/index/registration.pug'

    cache_max_age = 60

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['countries'] = Country.objects.all()
        return context


class ContentPageView(MenuContextMixin, CacheMixin, DetailView):
    template_name = 'pages/index/landing.pug'
    model = ContentPage
    context_object_name = 'page'
    queryset = ContentPage.objects.filter(is_published=True)

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        obj = self.get_object()
        # страница с доступом только авторизованным
        if obj.auth_only and not request.user.is_authenticated:
            return login_page_view(request)
        return response

