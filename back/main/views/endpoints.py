from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from main.serializers.user import RegisterSerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from cities_light.loading import get_cities_models


Country, Region, City = get_cities_models()


def get_cities_by_country(request):
    country_iso = request.GET.get('country_iso', '')
    country = get_object_or_404(Country, code2=country_iso.upper())

    cities = []
    for city in City.objects.filter(country=country):
        cities.append({
            'id': city.id,
            'name': city.name,
            'alternate_names': city.alternate_names
        })

    return JsonResponse({'country_iso': country_iso, 'cities': cities})


class RegistrationView(generics.CreateAPIView):
    """
    Регистрация пользователя
    """
    permission_classes = ()
    authentication_classes = ()
    serializer_class = RegisterSerializer
