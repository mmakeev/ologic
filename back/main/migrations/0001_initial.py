# Generated by Django 2.1.7 on 2019-02-24 15:04

import autoslug.fields
import cities_light.abstract_models
import cities_light.validators
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import main.models.user
import main.models.webinar
import mptt.fields
import sorl.thumbnail.fields
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('username', models.CharField(blank=True, max_length=150, null=True, verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='email address')),
                ('is_email_verified', models.BooleanField(default=False, verbose_name='email подтвержден')),
                ('phone', models.CharField(blank=True, default='', max_length=30, null=True)),
                ('is_phone_verified', models.BooleanField(default=False)),
                ('password_changed_at', models.DateTimeField(blank=True, null=True)),
                ('about', models.TextField(blank=True, default='')),
                ('avatar', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='avatars')),
                ('gender', models.CharField(blank=True, choices=[('male', 'male'), ('female', 'female')], max_length=10, null=True)),
                ('birthday', models.DateField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', main.models.user.UserModelManager()),
            ],
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('name_ascii', models.CharField(blank=True, db_index=True, max_length=200)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='name_ascii')),
                ('geoname_id', models.IntegerField(blank=True, null=True, unique=True)),
                ('alternate_names', models.TextField(blank=True, default='', null=True)),
                ('display_name', models.CharField(max_length=200)),
                ('search_names', cities_light.abstract_models.ToSearchTextField(blank=True, db_index=True, default='', max_length=4000)),
                ('latitude', models.DecimalField(blank=True, decimal_places=5, max_digits=8, null=True)),
                ('longitude', models.DecimalField(blank=True, decimal_places=5, max_digits=8, null=True)),
                ('population', models.BigIntegerField(blank=True, db_index=True, null=True)),
                ('feature_code', models.CharField(blank=True, db_index=True, max_length=10, null=True)),
                ('timezone', models.CharField(blank=True, db_index=True, max_length=40, null=True, validators=[cities_light.validators.timezone_validator])),
                ('order_number', models.PositiveIntegerField(default=0)),
            ],
            options={
                'verbose_name': 'Город',
                'verbose_name_plural': 'Города',
                'ordering': ['order_number'],
            },
        ),
        migrations.CreateModel(
            name='ContentPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300, verbose_name='заголовок')),
                ('slug', models.SlugField(unique=True)),
                ('html', tinymce.models.HTMLField()),
                ('exclude_from_sitemap', models.BooleanField(default=False, verbose_name='исключить из sitemap')),
                ('auth_only', models.BooleanField(default=False, verbose_name='только для авторизованных')),
                ('is_published', models.BooleanField(default=False, verbose_name='опубликовать')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='последнее обновление')),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='автор')),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='main.ContentPage', verbose_name='родитель')),
            ],
            options={
                'verbose_name': 'Страница',
                'verbose_name_plural': 'Страницы',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('name_ascii', models.CharField(blank=True, db_index=True, max_length=200)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='name_ascii')),
                ('geoname_id', models.IntegerField(blank=True, null=True, unique=True)),
                ('alternate_names', models.TextField(blank=True, default='', null=True)),
                ('code2', models.CharField(blank=True, max_length=2, null=True, unique=True)),
                ('code3', models.CharField(blank=True, max_length=3, null=True, unique=True)),
                ('continent', models.CharField(choices=[('OC', 'Oceania'), ('EU', 'Europe'), ('AF', 'Africa'), ('NA', 'North America'), ('AN', 'Antarctica'), ('SA', 'South America'), ('AS', 'Asia')], db_index=True, max_length=2)),
                ('tld', models.CharField(blank=True, db_index=True, max_length=5)),
                ('phone', models.CharField(blank=True, max_length=20, null=True)),
                ('order_number', models.PositiveIntegerField(default=0)),
            ],
            options={
                'verbose_name': 'Страна',
                'verbose_name_plural': 'Страны',
                'ordering': ['order_number'],
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(help_text='используется для красивых url и как источник в CRM', unique=True)),
                ('name', models.CharField(max_length=100, verbose_name='название')),
                ('description', models.TextField(blank=True, default='', verbose_name='описание')),
                ('date', models.DateTimeField(verbose_name='дата')),
                ('duration', models.DurationField(blank=True, null=True, verbose_name='продолжительность')),
                ('external_link', models.URLField(blank=True, help_text='на вебинар/страницу ВК/внутренню страницу', null=True, verbose_name='ссылка на событие')),
                ('image', sorl.thumbnail.fields.ImageField(null=True, upload_to='events', verbose_name='картинка')),
                ('is_published', models.BooleanField(default=True, verbose_name='опубликовано')),
            ],
            options={
                'verbose_name': 'событие',
                'verbose_name_plural': 'события',
                'ordering': ['-date'],
            },
        ),
        migrations.CreateModel(
            name='List',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('slug', models.SlugField()),
                ('is_default', models.BooleanField(default=False, help_text='список куда падают пользователи при создании, создается автоматически при создании школы')),
            ],
            options={
                'verbose_name': 'Список',
                'verbose_name_plural': 'Списки',
            },
        ),
        migrations.CreateModel(
            name='MediaFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to='files')),
                ('size_bytes', models.PositiveIntegerField(default=0, editable=False, null=True)),
                ('description', models.CharField(blank=True, default='', max_length=300)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('social', models.URLField(blank=True, default='', help_text='url на профиль в соцсети', null=True)),
                ('is_subscribed', models.BooleanField(default=False, help_text='получает рассылки или нет', verbose_name='получает рассылки')),
                ('source', models.CharField(blank=True, editable=False, help_text='откуда пришел пользователь, подставляется автоматически', max_length=300, null=True, verbose_name='источник')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='добавлен')),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('list', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='main.List')),
            ],
            options={
                'verbose_name': 'Ученик',
                'verbose_name_plural': 'Ученики',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('name_ascii', models.CharField(blank=True, db_index=True, max_length=200)),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='name_ascii')),
                ('geoname_id', models.IntegerField(blank=True, null=True, unique=True)),
                ('alternate_names', models.TextField(blank=True, default='', null=True)),
                ('display_name', models.CharField(max_length=200)),
                ('geoname_code', models.CharField(blank=True, db_index=True, max_length=50, null=True)),
                ('order_number', models.PositiveIntegerField(default=0)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Country')),
            ],
            options={
                'verbose_name': 'Регионы',
                'verbose_name_plural': 'Регион',
                'ordering': ['order_number'],
            },
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('slug', models.SlugField(unique=True)),
                ('description', models.TextField(blank=True, default='', null=True)),
                ('image', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='schools')),
                ('is_active', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('owner', models.ForeignKey(help_text='владелец школы', null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Онлайн школа',
                'verbose_name_plural': 'Онлайн школы',
            },
        ),
        migrations.CreateModel(
            name='Webinar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300, verbose_name='название')),
                ('slug', models.SlugField(help_text='используется для красивых url и как источник в CRM', unique=True)),
                ('description', models.TextField(blank=True, default='', null=True, verbose_name='описание')),
                ('image', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='schools', verbose_name='картинка')),
                ('time_start', models.TimeField(verbose_name='время начала')),
                ('is_published', models.BooleanField(default=False, verbose_name='опубликован')),
                ('video_url', models.URLField(help_text='ссылка на youtube или платформу для живых вебинаров', verbose_name='видео')),
                ('video_seconds', models.IntegerField(blank=True, help_text='в секундах', null=True, verbose_name='продолжительность видео')),
                ('moderator_name', models.CharField(max_length=50, verbose_name='имя модератора в чате')),
                ('chatlog_dt', models.CharField(blank=True, help_text='необходимо чтобы правильно привязать время сообщений к вебинару', max_length=30, null=True, validators=[main.models.webinar.datetime_field_validator], verbose_name='Время начала вебинара в файле лога')),
                ('chatlog_file', models.FileField(blank=True, help_text='при загрзке старый чат будет удален', null=True, upload_to='chatlogs', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['csv'])], verbose_name='CSV лог чата')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('host', models.ForeignKey(help_text='кто проводит вебинар', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='ведущий')),
                ('school', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.School', verbose_name='онлайн школа')),
            ],
            options={
                'verbose_name': 'Вебинар',
                'verbose_name_plural': 'Вебинары',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='WebinarChatMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.CharField(help_text='через сколько секунд показывать сообщение', max_length=9, validators=[main.models.webinar.time_field_validator], verbose_name='время')),
                ('name', models.CharField(max_length=50, verbose_name='имя')),
                ('message', models.TextField(verbose_name='сообщение')),
                ('webinar', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Webinar')),
            ],
            options={
                'ordering': ['time'],
            },
        ),
        migrations.CreateModel(
            name='WebinarLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.CharField(help_text='через сколько секунд показывать сообщение', max_length=9, validators=[main.models.webinar.time_field_validator], verbose_name='время')),
                ('name', models.CharField(max_length=1024, verbose_name='название ссылки')),
                ('url', models.URLField(verbose_name='ссылка')),
                ('webinar', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Webinar')),
            ],
            options={
                'ordering': ['time'],
            },
        ),
        migrations.AddField(
            model_name='person',
            name='school',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='main.School', verbose_name='школа'),
        ),
        migrations.AddField(
            model_name='person',
            name='user',
            field=models.ForeignKey(blank=True, editable=False, help_text='связывается с пользователем в момент подтверждения email', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='профиль на сайте'),
        ),
        migrations.AddField(
            model_name='list',
            name='school',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.School'),
        ),
        migrations.AddField(
            model_name='event',
            name='school',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.School', verbose_name='школа'),
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Country'),
        ),
        migrations.AddField(
            model_name='city',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.Region'),
        ),
        migrations.AddField(
            model_name='user',
            name='city',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='main.City'),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
        migrations.AlterUniqueTogether(
            name='person',
            unique_together={('user', 'school')},
        ),
        migrations.AlterUniqueTogether(
            name='list',
            unique_together={('slug', 'school')},
        ),
    ]
