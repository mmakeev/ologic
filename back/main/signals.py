import logging
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from main.models import User, School, List


logger = logging.getLogger(__name__)


@receiver(pre_save, sender=User)
def username_fix(sender, instance, **kwargs):
    """
    Фикс на всякий случай для совместимости со всякими 3rd party штуками
    по умолчанию используем email для логина и всего остального
    """
    # email всегда равен username, чтобы не сломалась обратная совместимость
    # при создании из админки наоборот надо email = username
    if instance.username and not instance.email:
        instance.email = instance.username
    else:
        instance.username = instance.email


@receiver(post_save, sender=School)
def create_default_lists(sender, instance, created, **kwargs):
    """
    Создаем списки для вновь добавленной школы
    """
    if not created:
        return

    # список по умолчанию
    List.objects.create(school=instance, name='входящие', slug='inbox', is_default=True)
    # дополнительные списки
    List.objects.create(school=instance, name='горячие', slug='hot')
    List.objects.create(school=instance, name='холодные', slug='cold')
    List.objects.create(school=instance, name='шлак', slug='junk')
    List.objects.create(school=instance, name='разное', slug='other')
