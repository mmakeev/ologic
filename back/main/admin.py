import csv
import datetime
from django.conf import settings
from functools import partial
from django.contrib import admin
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from project.admin import custom_admin_site
from sorl.thumbnail.admin import AdminImageMixin
from sorl.thumbnail import get_thumbnail
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django.contrib.admin.filters import DateFieldListFilter
from django.core.exceptions import PermissionDenied
from django.db import models
from django.urls import reverse
from project.helpers.stuff import url_to_edit, sizeof_fmt
from admin_comments.admin import CommentInline
from sorl.thumbnail.admin import AdminImageMixin
from mptt.admin import MPTTModelAdmin
from boolean_switch.admin import AdminBooleanMixin
from django.forms.models import (
    modelformset_factory, BaseModelFormSet
)
from cities_light.admin import CountryAdmin, RegionAdmin, CityAdmin
from cities_light.loading import get_cities_models
from adminsortable2.admin import SortableAdminMixin

from main.models import (
    MediaFile, School, List, Person, Webinar, WebinarChatMessage,
    WebinarLink, Event, ContentPage
)


Country, Region, City = get_cities_models()


CommentInline.classes = []  # removing collapse class


class CustomDateFieldListFilter(DateFieldListFilter):
    def __init__(self, field, request, params, model, model_admin, field_path):
        super(CustomDateFieldListFilter, self).__init__(field, request, params, model, model_admin, field_path)

        now = timezone.now()
        # When time zone support is enabled, convert "now" to the user's time
        # zone so Django's definition of "Today" matches what the user expects.
        if timezone.is_aware(now):
            now = timezone.localtime(now)

        if isinstance(field, models.DateTimeField):
            today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        else:       # field is a models.DateField
            today = now.date()
        tomorrow = today + datetime.timedelta(days=1)
        if today.month == 12:
            next_month = today.replace(year=today.year + 1, month=1, day=1)
        else:
            next_month = today.replace(month=today.month + 1, day=1)
        next_year = today.replace(year=today.year + 1, month=1, day=1)

        self.lookup_kwarg_since = '%s__gte' % field_path
        self.lookup_kwarg_until = '%s__lt' % field_path
        self.links = (
            (_('Any date'), {}),
            (_('Today'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('Past 7 days'), {
                self.lookup_kwarg_since: str(today - datetime.timedelta(days=7)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('Next 7 days'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(today + datetime.timedelta(days=7)),
            }),
            (_('This month'), {
                self.lookup_kwarg_since: str(today.replace(day=1)),
                self.lookup_kwarg_until: str(next_month),
            }),
            (_('This year'), {
                self.lookup_kwarg_since: str(today.replace(month=1, day=1)),
                self.lookup_kwarg_until: str(next_year),
            }),
        )


@admin.register(MediaFile, site=custom_admin_site)
class MediaFileAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'size', 'description', 'created_at', 'updated_at']
    search_fields = ['file', 'description']
    actions_on_top = False
    actions_on_bottom = False

    def size(self, obj):
        return sizeof_fmt(obj.size_bytes)


@admin.register(School, site=custom_admin_site)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'owner', 'is_active']
    search_fields = ['name', 'description']
    prepopulated_fields = {'slug': ('name',), }
    actions_on_top = False
    actions_on_bottom = False

    def has_add_permission(self, request, obj=None):
        return request.user.is_superuser


@admin.register(List, site=custom_admin_site)
class ListAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'school', 'is_default']
    list_filter = ['school']
    prepopulated_fields = {'slug': ('name',), }

    ###
    # создавать, удалять и редактировать списки пока может только админ
    # у владельца школы есть доступ сюда через list_editable, поэтому надо явно ограничивать права
    ###
    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_edit_permission(self, request):
        return request.user.is_superuser

    def get_queryset(self, request):
        """
        У админка есть права ко всем спискам, у остальных только к их школе
        """
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs

        active_school_id = request.session.get('active_school_id')
        if not active_school_id:
            active_school = School.objects.filter(owner=request.user).first().id

        try:
            active_school = School.objects.get(id=active_school_id, owner=request.user)
        except School.DoesNotExist:
            raise PermissionDenied('school not exsits or you are not owner')

        qs = qs.filter(school=active_school)
        return qs


class ChatMessagesInline(admin.TabularInline):
    model = WebinarChatMessage
    extra = 0
    verbose_name = 'сообщение'
    verbose_name_plural = 'Чат'
    classes = ['collapse']


class ChatLinksInline(admin.TabularInline):
    model = WebinarLink
    extra = 0
    verbose_name = 'ссылка'
    verbose_name_plural = 'ссылки'
    classes = ['collapse']


@admin.register(Webinar, site=custom_admin_site)
class WebinarAdmin(AdminBooleanMixin, admin.ModelAdmin):
    change_form_template = 'admin/webinar_change_view.html'
    # @TODO вывести тип и дату/время одним полем
    list_display = ['name', 'school', 'is_published']
    list_filter = ['school', 'host']
    prepopulated_fields = {'slug': ('name',), }
    inlines = [ChatMessagesInline, ChatLinksInline]
    save_as = True

    def save_form(self, request, form, change):
        """
        Пытаемся тут распарсить и сохранить файл чата
        """
        instance = form.save(commit=False)

        if request.FILES.get('csv_log_file') and request.POST.get('csv_log_time_start'):
            # тут надо распарсить время из первого столбца и корректно сохранить в базу
            # время в файле будет с датой 07.02.2019 21:08:14
            reader = csv.reader(request.FILES['csv_log_file'].read().decode('utf-8').splitlines())
            webinar_start = datetime.datetime.strptime(request.POST['csv_log_time_start'], '%d.%m.%Y %H:%M:%S')

            # удаляем старый чат
            WebinarChatMessage.objects.filter(webinar=instance).delete()
            for line in reader:
                # пытаемся распарсить время из даты
                try:
                    t = datetime.datetime.strptime(line[0], '%d.%m.%Y %H:%M:%S')
                except ValueError:
                    # какая-то левая строка(заголовок или комментарий), пропускаем
                    continue

                name = line[1].strip()
                message = line[2].strip()

                if not name or not message:
                    continue

                # считаем относительное время, без учета даты
                relevant_dt = int(t.timestamp() - webinar_start.timestamp())
                if relevant_dt < 0:
                    formated_time = '-0%s' % str(datetime.timedelta(seconds=int(abs(relevant_dt))))
                else:
                    formated_time = '0%s' % str(datetime.timedelta(seconds=int(relevant_dt)))

                WebinarChatMessage.objects.create(
                    webinar=instance,
                    time=formated_time,
                    name=name,
                    message=message
                )

        return instance


def move_to_list(modeladmin, request, queryset, list_id):
    queryset.update(list_id=list_id)


@admin.register(Person, site=custom_admin_site)
class PersonAdmin(AdminBooleanMixin, admin.ModelAdmin):
    change_list_template = 'admin/persons_list.html'
    list_display = ['user', 'list', 'source', 'get_last_comment', 'is_subscribed',
                    'is_email_verified', 'created_at']
    list_editable = ['list']
    list_filter = ['source', 'is_subscribed', 'user__is_email_verified', 'created_at']
    date_hierarchy = 'created_at'
    inlines = [CommentInline]
    search_fields = ['=id', 'user__email', 'user__phone', 'user__last_name',
                     'user__first_name']
    readonly_fields = ['school']
    actions = []

    def has_delete_permission(self, request, obj=None):
        # запрещаем удалять учеников
        return False

    def get_actions(self, request):
        """
        Добавляем action на каждый список, чтобы избежать промежуточных страниц
        """
        actions = super().get_actions(request)

        # убираем опц
        # if 'delete_selected' in actions:
        #         del actions['delete_selected']

        for l in self.lists:
            new_action = partial(move_to_list, list_id=l.id)
            name = 'move_to_list_%s' % l.slug
            actions.update({name: (new_action, name, 'Переместить в список "%s"' % l.name)})

        return actions

    def has_add_permission(self, request, obj=None):
        """
        Пользователей в школу пока нельзя добавлять вручную
        """
        return request.user.is_superuser

    def is_email_verified(self, obj):
        return obj.user.is_email_verified if obj.user else False
    is_email_verified.boolean = True
    is_email_verified.short_description = 'Email подтвержден'

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Тут управляем какие списки доступы для редактирвоания пользователю
        _get_extra_context уже вызван и у нас есть доступ к школе пользователя
        """
        formfield = super().formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == 'list':
            formfield.queryset = formfield.queryset.filter(school=self.active_school)
        return formfield

    def _get_extra_context(self, request):
        """
        Контекст для страниц
        """
        # dict id => obj
        self.user_schools = School.objects.filter(owner=request.user)
        if not self.user_schools:
            raise PermissionDenied('user have not schools')

        # пользователь может работать только с одной школой за раз
        active_school_id = request.session.get('active_school_id')
        try:
            self.active_school = self.user_schools.get(id=active_school_id, owner=request.user) if active_school_id else self.user_schools.first()
        except School.DoesNotExist:
            raise PermissionDenied('school not exsits or you are not owner')

        # списки доступные пользователю/онлайн-школе
        self.lists = List.objects.filter(school=self.active_school)
        self.user_schools = self.user_schools
        # список по умолчанию
        self.default_list = List.objects.get(school=self.active_school, is_default=True)

        # активный список
        active_list_id = request.GET.get('list__id__exact')
        try:
            self.active_list = List.objects.get(id=int(active_list_id), school=self.active_school) if active_list_id else self.default_list
        except List.DoesNotExist:
            # пришел какой-то левый айдишник списка, просто показываем список по умолчанию
            self.active_list = self.default_list

    def get_queryset(self, request):
        """
        Выводим только пользователей определенного списка и школы
        """
        qs = super().get_queryset(request)
        qs = qs.select_related('user')
        if not hasattr(self, 'active_list'):
            self._get_extra_context(request)
        # подставляес активный список
        qs = qs.filter(list=self.active_list)
        return qs

    def changelist_view(self, request, extra_context=None):
        """
        Добавляем наш контекст
        """
        extra_context = extra_context or {}
        # этот метод вызывается перед get_queryset, поэтому можем здесь собрать
        # данные о пользователе заранее и потом использовать
        self._get_extra_context(request)
        extra_context['lists'] = self.lists
        extra_context['user_schools'] = self.user_schools
        extra_context['default_list'] = self.default_list
        extra_context['active_list'] = self.active_list
        extra_context['active_school'] = self.active_school

        # активный список
        active_list_id = request.GET.get('list__id__exact')
        try:
            self.active_list = List.objects.get(id=int(active_list_id), school=self.active_school) if active_list_id else extra_context['default_list']
        except List.DoesNotExist:
            # пришел какой-то левый айдишник списка, просто показываем список по умолчанию
            self.active_list = extra_context['default_list']

        extra_context['active_list'] = self.active_list

        return super().changelist_view(request, extra_context=extra_context)


@admin.register(Event, site=custom_admin_site)
class EventAdmin(AdminImageMixin, AdminBooleanMixin, admin.ModelAdmin):
    list_display = ['name', 'slug', 'school', 'date', 'is_published']
    list_filter = ['school']
    prepopulated_fields = {'slug': ('name',), }
    actions_on_top = False
    actions_on_bottom = False
    actions = None
    save_as = True


@admin.register(Country, site=custom_admin_site)
class CustomCountryAdmin(SortableAdminMixin, CountryAdmin):
    list_editable = (
        'name',
        'code2',
        'code3',
        'phone',
        'geoname_id',
    )
    list_display = (
        'id',
        'cities',
        'name',
        'code2',
        'code3',
        'phone',
        'geoname_id',
    )
    list_display_links = (
        'id',
    )

    def cities(self, obj):
        cities_url = reverse('admin:main_city_changelist')
        return mark_safe('<a class="grp-button" href="{}?country__id__exact={}" target="blank">Города</a>'.format(cities_url, obj.id))


@admin.register(Region, site=custom_admin_site)
class CustomRegionAdmin(SortableAdminMixin, RegionAdmin):
    list_editable = (
        'name',
        'country',
        'geoname_id',
    )
    list_display = (
        'id',
        'name',
        'country',
        'geoname_id',
    )
    list_display_links = (
        'id',
    )


@admin.register(City, site=custom_admin_site)
class CustomCityAdmin(SortableAdminMixin, CityAdmin):
    list_editable = (
        'name',
        'region',
        'country',
        'geoname_id',
        'timezone'
    )
    list_display = (
        'id',
        'name',
        'region',
        'country',
        'geoname_id',
        'timezone'
    )
    list_display_links = (
        'id',
    )


@admin.register(ContentPage, site=custom_admin_site)
class ContentPageAdmin(AdminBooleanMixin, MPTTModelAdmin):
    list_display = ['title', 'url', 'exclude_from_sitemap', 'auth_only',
                    'is_published', 'created_at']
    list_filter = ['is_published', 'exclude_from_sitemap', 'auth_only']
    search_fields = ['title', 'slug', '=pk']
    date_hierarchy = 'created_at'
    prepopulated_fields = {'slug': ('title',), }

    def url(self, obj):
        return '<a href="{url}" target="_blank">/{slug}/</a>'.format(
            url=obj.get_absolute_url(),
            slug=obj.slug
        )
    url.allow_tags = True
