from django_cron import CronJobBase, Schedule
# from main.tasks.cron import (
#     sync_wallets_with_bank,
# )


class CurrenciesUpdateFiat(CronJobBase):
    RUN_EVERY_MINS = 60
    MIN_NUM_FAILURES = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'currencies_update_fiat'

    def do(self):
        # do something here
        pass
