import names
import math
import sys
import requests
import collections
import random
import re
import logging
from urllib.error import HTTPError
from urllib.parse import quote
from string import ascii_lowercase
from django.conf import settings
from io import BytesIO
from PIL import Image, ImageDraw
from datetime import timedelta, datetime, date
from django.core.management.base import BaseCommand
from django.core.files import File
from django.utils.lorem_ipsum import words
from django.utils.text import slugify
from django.core import management
from sorl.thumbnail import get_thumbnail
from django.contrib.auth.models import Group
from transliterate import translit
from main.models.cities_light import City
from main.models import (
    User, School, Person, List
)
from blogs.models import (
    Post, Category
)
from shop.models import (
    Goods, GoodsImage
)

RGBColor = collections.namedtuple('RGBColor', ['red', 'green', 'blue'])

logger = logging.getLogger(__name__)


def random_color(start, end):
    while True:
        chosen_d = random.uniform(0, 1)
        yield RGBColor(
            start.red - int((start.red - end.red) * chosen_d),
            start.green - int((start.green - end.green) * chosen_d),
            start.blue - int((start.blue - end.blue) * chosen_d)
        )


def _scale_coordinates(generator, image_width, image_height, side_length=50):
    scaled_width = int(image_width / side_length) + 1
    scaled_height = int(image_height / side_length) + 1
    for coords in generator(scaled_width, scaled_height):
        yield [(x * side_length, y * side_length) for (x, y) in coords]


def generate_unit_triangles(image_width, image_height):
    h = math.sin(math.pi / 3)
    for x in range(-1, image_width):
        for y in range(int(image_height / h)):
            x_ = x if (y % 2 == 0) else x + 0.5
            yield [(x_, y * h), (x_ + 1, y * h), (x_ + 0.5, (y + 1) * h)]
            yield [(x_ + 1, y * h), (x_ + 1.5, (y + 1) * h), (x_ + 0.5, (y + 1) * h)]


def generate_triangles(*args, **kwargs):
    return _scale_coordinates(generate_unit_triangles, *args, **kwargs)


def get_random_name():
    length = random.randint(5, 10)
    return ''.join(random.choice(ascii_lowercase[:12]) for i in range(length))


def generate_random_email():
    return '%s@%s' % (
        get_random_name(),
        random.choice(['yandex.com', 'gmail.com', 'mail.com', 'mail.ru', 'yahoo.com'])
    )


def hsv_to_rgb(h, s, v):
    if s == 0.0: return v, v, v
    i = int(h*6.0)
    f = (h*6.0) - i
    p = int(v*(1.0 - s))
    q = int(v*(1.0 - s*f))
    t = int(v*(1.0 - s*(1.0-f)))
    i = i%6
    if i == 0: return v, t, p
    if i == 1: return q, v, p
    if i == 2: return p, v, t
    if i == 3: return p, q, v
    if i == 4: return t, p, v
    if i == 5: return v, p, q


def random_image(x, y):
    """
    Generate the shapes and colors, and draw them on the canvas
    """
    im = Image.new(mode='RGB', size=(x, y))

    huy = random.random()
    huy2 = huy + 0.8
    if huy2 > 1:
        huy2 = huy2 - 1

    c1 = hsv_to_rgb(huy, 1, 120)
    c2 = hsv_to_rgb(huy2, 0.6, 250)

    color1 = RGBColor(*c1)
    color2 = RGBColor(*c2)

    shapes = generate_triangles(int(x * 1.2), int(y * 1.2), int(x / 5))
    colors = random_color(color1, color2)
    for shape, color in zip(shapes, colors):
        ImageDraw.Draw(im).polygon(shape, fill=color)

    return im


def get_file(path=None, allow_download=False, file_type=None):
    """
    Создает или скачивает файл для объекта модели,
    зависит от allow_download
    """
    if file_type is None:
        file_type = 'image'

    if path and path[:4] == 'http' and allow_download:
        path = quote(path, safe=':/?*=\'')
        logger.debug('Loading: %s' % path)
        try:
            u = requests.get(path)
            f = BytesIO()
            f.write(u.content)
            return File(f)
        except HTTPError:
            logger.error('HTTPError for file: %s' % path)
            return File(BytesIO())

    if file_type == 'image':
        image_file = random_image(random.randint(200, 300), random.randint(300, 400))
        blob = BytesIO()
        image_file.save(blob, 'PNG')
        return File(blob)
    elif file_type == 'pdf':
        # @TODO можно заменить на какой-то локальный рандомный файл
        path = 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf'
        u = requests.get(path)
        f = BytesIO()
        f.write(u.content)
        return File(f)
    else:
        raise Exception('unknown file_type')


def get_text_data_post(theme, number):
    url = 'https://yandex.ru/referats/?t={}&s={}'.format(theme, number)
    result = requests.get(url)
    result_text = result.text
    data_searches = re.search(
        r'<div class="referats__text">.*<strong>(.*)</strong>((<p>.*?</p>)<p>.*?</p>)</div>',
        result_text
    )
    try:
        text_data = {
            'title': data_searches.group(1),
            'slug': slugify(translit(data_searches.group(1), reversed=True)),
            'annotation': data_searches.group(3),
            'text': data_searches.group(2)
        }
    except AttributeError:
        return get_text_data_post(theme, number+1)
    return text_data


class Command(BaseCommand):
    help = 'Создание тестовых данных в базе'

    def add_arguments(self, parser):
        parser.add_argument(
            '--allow-download', action='store_true', dest='allow_download', default=False,
            help='Download files and images from http sources',
        )

    def handle(self, allow_download, *args, **options):

        if not settings.DEBUG:
            logger.error('Can’t run generate in production mode')
            sys.exit(1)

        # load Groups
        management.call_command('loaddata', 'groups', format='json', verbosity=0)

        if not User.objects.filter(email='admin@online-logic.com').exists():
            u1 = User.objects.create_user(
                username='admin@online-logic.com',
                email='admin@online-logic.com',
                password='1',
                first_name='Admin',
                last_name='Adminovitch',
                is_superuser=True,
                is_staff=True,
                is_active=True,
                is_email_verified=True,
            )
            u1.save()

            logger.info('admin@online-logic.com created')

        try:
            u2 = User.objects.get(email='user@online-logic.com')
        except User.DoesNotExist:
            u2 = User.objects.create_user(
                username='user@online-logic.com',
                email='user@online-logic.com',
                password='1',
                first_name='Юзер',
                last_name='ЮзеровскийПотемкин',
                is_superuser=False,
                is_staff=True,
                is_active=True,
                is_email_verified=True,
            )
            u2.save()

            logger.info('user@online-logic.com created')

            # даем прав на вход в амиднку и управление школой
            school_owner_group = Group.objects.get(name='school_owner')
            school_owner_group.user_set.add(u2)

        # # Constance
        # management.call_command('loaddata', 'constance', format='json', verbosity=0)
        # management.call_command('loaddata', 'groups', format='json', verbosity=0)

        # Blogs
        management.call_command('loaddata', 'categories', format='json', verbosity=0)
        categories = Category.objects.all()
        users = User.objects.all()
        themes = ['mathematics', 'marketing', 'law', 'psychology', 'physics']
        counter = 0
        for n in range(40):
            number = random.randint(1, 100)
            theme = random.choice(themes)
            post = Post(**get_text_data_post(theme, number))
            if categories.filter(slug=theme).exists():
                category = categories.get(slug=theme)
            else:
                category = random.choice(categories)
            post.category = category
            post.user = random.choice(users)
            post.is_published = random.choice([True, False])
            post.save()
            post.tags.add('tag_{}'.format(category.name.lower()))
            counter += 1
        logger.info('%d blog posts created' % counter)

        schools = []  # список доступных школ
        # школа по умолчанию
        try:
            s = School.objects.get(slug='tlc')
        except School.DoesNotExist:
            s = School.objects.create(
                name='онлайн школа TLC', slug='tlc',
                description='онлайн школа проекта TLC',
                owner=User.objects.first(), is_active=True
            )
        schools.append(s)

        # генерим еще онлайн школ для массовки
        # списки создаются автоматически по сигналу
        for n in range(5):
            name = words(random.randint(2, 4), common=False)
            s = School.objects.create(
                name=name, slug=slugify(name),
                description=words(15, common=False), is_active=True
            )
            schools.append(s)
        logger.info('%d schools created' % len(schools))

        # дарим школу нашем юзеру
        random_school = random.choice(schools)
        random_school.owner = u2
        random_school.save()

        # генерим учеников
        cities = list(City.objects.all().order_by('?')[:50])
        counter = 0
        for n in range(500):
            username = generate_random_email()
            u = User.objects.create_user(
                username=username,
                email=username,
                first_name=names.get_first_name(),
                last_name=names.get_last_name(),
                password='1',
                gender=random.choice(['male', 'female']),
                city=random.choice(cities)
            )
            # определяем пользователя в рандомную школу
            school = random.choice(schools)
            lst = List.objects.filter(school=school).order_by('?').first()
            Person.objects.create(
                user=u, school=school, list=lst, is_subscribed=random.randint(1, 3) == 2
            )
            counter += 1
        logger.info('%d student users created' % counter)
        # товары для магазина
        goods_titles = [
            'Часы', 'Автомобиль', 'Сиреневенькая глазовыколупывалка'
        ]
        for title in goods_titles:
            try:
                goods = Goods.objects.get(name=title)
            except Goods.DoesNotExist:
                goods = Goods.objects.create(
                    name=title,
                    slug=slugify(translit(title, reversed=True)),
                    goods_type='letter',
                    description='Lorem ipsum dolor sit amet, consectetur '
                                'adipisicing elit. Ipsam porro suscipit enim '
                                'optio fugiat amet labore cum id, hic facilis!',
                    price=random.uniform(10, 1000),
                    partner_reward=random.randint(1, 100),
                    is_published=True
                )

                logger.info('Created goods id:{}'.format(goods.id))

                for n in range(5):
                    goods_image = GoodsImage(goods=goods)
                    goods_image.photo.save('goods.jpg', get_file(
                            # path='http://gala-sport.ru/filecache/cache/b719066159bedb642e24fefc4f7ae3ec.png',
                            # allow_download=True
                        ))
                    goods_image.save()

        logger.info('All Done')
