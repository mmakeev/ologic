from main.models import User, Person
from django.contrib.auth import get_user_model, authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers


class RegisterSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    phone = serializers.CharField(required=True)
    password = serializers.CharField(write_only=True)
    social = serializers.URLField(write_only=True, required=False)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'gender', 'birthday',
                  'phone', 'city', 'password', 'social']
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True},
            'email': {'required': True},
            'gender': {'required': True},
            'birthday': {'required': True},
            'phone': {'required': True},
        }

    def create(self, validated_data):
        # онлайн школы нет при регистрации с сайта
        user = User.objects.create(
            email=validated_data['email'],
            phone=validated_data['phone'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            gender=validated_data['gender'],
            birthday=validated_data['birthday'],
            city=validated_data['city'],
        )
        user.set_password(validated_data['password'])
        user.save()

        # создаем Person без списка и школы
        Person.objects.create(
            user=user,
            is_subscribed=False,  # не подписываем, пока не подтвердит
            social=validated_data.get('social', ''),
        )

        return user
