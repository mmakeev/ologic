from django.conf import settings
from django.db import models
from django.db.models.query import QuerySet
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from tinymce import HTMLField
from taggit.managers import TaggableManager


class Category(models.Model):
    name = models.CharField('название категории', max_length=120)
    slug = models.SlugField(_('slug'), max_length=140, unique=True)

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'
        ordering = ['name']

    def __str__(self):
        return self.name


class PostMixin(object):
    def published(self):
        return self.filter(
            is_published=True
        )

    def unpublished(self):
        return self.filter(
            is_published=False
        )


class PostQuerySet(QuerySet, PostMixin):
    pass


class PostManager(models.Manager, PostMixin):
    def get_queryset(self):
        return PostQuerySet(self.model, using=self._db)


class Post(models.Model):
    title = models.CharField('заголовок', max_length=210)
    slug = models.SlugField(_('slug'), max_length=255)
    annotation = HTMLField('выдержка')
    text = HTMLField('текст поста')
    is_published = models.BooleanField('опубликован', default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             verbose_name='автор')
    category = models.ForeignKey(Category, on_delete=models.CASCADE,
                                 verbose_name='категория')
    created_at = models.DateTimeField('дата создания', auto_now_add=True)
    updated_at = models.DateTimeField('дата изменения', auto_now=True)
    publication_date = models.DateTimeField('дата публикации', null=True, blank=True)

    tags = TaggableManager(blank=True)
    objects = PostManager()

    class Meta:
        verbose_name = 'пост'
        verbose_name_plural = 'посты'
        ordering = ['-created_at']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'username': self.user.username,
            'category_slug': self.category.slug,
            'slug': self.slug,
            'id': self.pk
        }
        return reverse('blogs:post-detail', kwargs=kwargs)
