from django.contrib import admin
from project.admin import custom_admin_site
from .models import Category, Post
from taggit.models import Tag


@admin.register(Category, site=custom_admin_site)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ['name', 'slug']
    list_display = ['name', 'slug']


@admin.register(Post, site=custom_admin_site)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'user', 'category', 'tag_list', 'created_at', 'updated_at', 'is_published', 'publication_date']
    date_hierarchy = 'publication_date'
    prepopulated_fields = {"slug": ("title",)}
    search_fields = ['title', 'slug']
    readonly_fields = ['user']
    autocomplete_fields = ['user']
    list_filter = ['category', 'is_published', 'tags']

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

    tag_list.short_description = 'метки'

    def get_readonly_fields(self, request, obj=None):
        ro_fields = super().get_readonly_fields(request, obj)
        if request.user.is_superuser:
            if 'user' in ro_fields:
                ro_fields.remove('user')
        else:
            if not 'user' in ro_fields:
                ro_fields.append('user')
        return ro_fields

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser and not obj.pk:
            obj.user = request.user
        super().save_model(request, obj, form, change)


@admin.register(Tag, site=custom_admin_site)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    search_fields = ['name']
