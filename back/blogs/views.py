from django.http import HttpResponse, Http404
from django.views.generic import ListView, DetailView
from .models import Post, Category
from main.models import User


class PostListView(ListView):
    model = Post
    paginate_by = 10
    queryset = Post.objects.published()
    template_name = 'pages/index/blog-list.pug'

    def head(self, *args, **kwargs):
        last_post = self.get_queryset().latest('publication_date')
        response = HttpResponse('')
        response['Last-Modified'] = last_post.publication_date.strftime(
            '%a, %d %b %Y %H:%M:%S GMT'
        )
        return response

    def get_queryset(self):
        qs = super().get_queryset()
        if 'username' in self.kwargs:
            username = self.kwargs.get('username')
            if not User.objects.filter(username=username).exists():
                raise Http404
            qs = qs.filter(user__username=username)
        if 'category_slug' in self.kwargs:
            category_slug = self.kwargs.get('category_slug')
            if not Category.objects.filter(slug=category_slug).exists():
                raise Http404
            qs = qs.filter(category__slug=category_slug)
        return qs


class PostDetalView(DetailView):
    model = Post
    queryset = Post.objects.published()
    template_name = 'pages/index/blog.pug'

    def get_queryset(self):
        qs = super().get_queryset()
        username = self.kwargs.get('username')
        category_slug = self.kwargs.get('category_slug')
        slug = self.kwargs.get('slug')
        id = self.kwargs.get('id')
        qs = qs.filter(id=id, slug=slug, category__slug=category_slug, user__username=username)
        return qs
