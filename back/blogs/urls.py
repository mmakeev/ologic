from django.urls import path
from .views import (PostListView, PostDetalView)


app_name = 'blogs'


urlpatterns = [
    path('', view=PostListView.as_view(), name='post-list'),
    path('<username>/', view=PostListView.as_view(), name='post-list'),
    path('<username>/<category_slug>/', view=PostListView.as_view(), name='post-list'),
    path('<username>/<category_slug>/<slug:slug>-<int:id>/', PostDetalView.as_view(), name='post-detail'),
]
