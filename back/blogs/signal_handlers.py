from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.utils import timezone
from .models import Post


@receiver(pre_save, sender=Post)
def set_publication_date(sender, instance, **kwargs):
    if instance.id:
        post = Post.objects.get(id=instance.id)
        if not post.is_published and instance.is_published:
            instance.publication_date = timezone.now()
        elif post.is_published and not instance.is_published:
            instance.publication_date = None
    elif not instance.id and instance.is_published:
        instance.publication_date = timezone.now()
    return
