**Install**

```
brew update
brew install pyenv
pyenv install 3.6.2
```

После этого надо проверить что находясь в папке проекта (там где лежит `.python-version`) правильная версия питона.
```
python --version
# Python 3.6.2
```
Если не сработало, можно попробовать перезапустить терминал.

Далее собираем виртуальное окружение:
```
sudo easy_install pip
pip install virtualenv
virtualenv .env
make install
```

Генерим базу перед первым запуском:
```
make generate
```

Запускаем сервер:
```
make runserver
```

**Другие полезные команды**

Полный реинстал с очисткой базу(сотрет файл sqlite, запустит install и generate):
```
make reinstall
```

Собирает билд фронта в статику:
```
make build-front
```

Авторизация и вход в админку тут http://127.0.0.1:8000/admin/
дефолтный юзер:
`admin@online-logic.com/1`