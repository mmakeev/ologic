$(function() {
	var $container = $('<div style="width:0;height:0;overflow:hidden"></div>').prependTo(document.body);

	var spritePath = '/static/img/svg/sprite.svg'

	$.get(spritePath, function (data) {
		$container.append(typeof XMLSerializer != 'undefined'
			? (new XMLSerializer()).serializeToString(data.documentElement)
			: $(data.documentElement).html());
	});
});