tippy.setDefaults({
	arrow: false,
	hideOnClick: false,
	delay: 40,
	placement: 'top-start',
	trigger: 'click focus'
});

/**
 * Валидация формы
 * 
 * @param  callback success_callback принимает json аргументом
 * @param  callback fail_callback  принимает аргументы (xhr, status, error)
 * @return void
 */
$.fn.ologic_validate = function(success_callback, fail_callback) {
	var $thisForm = $(this);
	var thisAction = $(this).attr('action');

	$thisForm.parsley({
		errorClass: '_error'
	});

	// сабмит формы
	$('body').on('submit', $thisForm, function(e) {
		e.preventDefault();
		$thisForm.find('.field-error').remove();

		$.post(thisAction, $thisForm.serialize(), function(json) {
			success_callback(json);
		}, 'json')
		.fail(function(xhr, status, error) {
			if (typeof fail_callback === "undefined") {
				if(xhr.status == 400) {
					var errorFields = JSON.parse(xhr.responseText);
					$.each(errorFields, function(i, item) {
						$thisForm.find('[name="' + i + '"]').after('<div class="field-error">' + item[0].message + '</div>');
					});
				}
			} else {
				fail_callback(xhr, status, error);
			}
		});
	});
};

$(function() {
	$('.calendar').datepicker({
		autoClose: true,
		maxDate: new Date(),
		dateFormat: "yyyy-mm-dd",
		altFormat: "dd.mm.yyyy"
	});

	$('.ologic-regform__promo-button').on('click', function(e) {
		$(this).stop().fadeOut(100, function() {
			$('input[name="ologic-reg-promo"]').attr('tabindex', 0).focus();
		});
	});

	// форма регистрации
	$('.ologic-regform').ologic_validate(function(json) {
		console.log(json);
		alert('успешная регистрация');
	});

	// выбор города по стране
	$('body').on('change', '#country', function(e) {
		$this = $(this);
		var country_iso = $(this).val();
		$.getJSON('/api/cities/get_by_country/?country_iso='+country_iso, function(json) {
			var $city = $('#city');
			$city.find('option').remove();
			$.each(json.cities, function(i, city) {
				$city.append(new Option(city.name, city.id))
			});
		});
	});

	// Other

	$('.ologic-hero__button a').on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#social_links').offset().top
		}, 500);
	});

	$('[data-login-link]').fancybox({
		helpers: {
			overlay: {
				locked: false
			}
		},
		margin: 0,
		padding: 0,
		closeBtn: false,
		'width': 340,
		'height': 'auto',
		'autoSize': false
	});

	$('[data-close-login]').on('click', function(e) {
		e.preventDefault();
		$.fancybox.close();
	});

	$('[data-open-mobile-menu]').on('click', function(e) {
		e.preventDefault();
		$('.ologic-menu').addClass('ologic-menu_active');
	});

	$('[data-close-mobile-menu]').on('click', function(e) {
		e.preventDefault();
		$('.ologic-menu').removeClass('ologic-menu_active');
	});

	$('a.js-open-text-popup').fancybox({
		'padding': 0,
		'autoScale' : false,
	});

	$('a.js-open-video').on('click', function(e) {
		e.preventDefault();
		$.fancybox({
			helpers: {
				overlay: {
					locked: false
				}
			},
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'embed/'),
			'type'			: 'iframe'
		});
	});


	$('.ologic-blog-author').stick_in_parent({
		offset_top: 25
	});

	var $window = $(window);

	function scrollParallax() {
		var scrollPos = $window.scrollTop();
		var imageScrollPos = (scrollPos);
		$('[data-parallax]').css({
			'transform': 'translateY(' + imageScrollPos + 'px)'
		});
	}


	// if($('[data-parallax]').length) {
	// 	scrollParallax();
	// 	var $parallaxElem = $('[data-parallax]');

	// 	$(window).on('scroll', function() {
	// 		scrollParallax();
	// 	});
	// }

});