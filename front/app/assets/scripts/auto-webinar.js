$(function() {
	var $webinarChat = $('.webinar-chat__content');

	if ($webinarChat.length) {
		var webinarChatHeight = $webinarChat.outerHeight();
		$webinarChat.scrollTop(webinarChatHeight);
	}
})