var gulp = require('gulp'),
	fs = require('fs'),
	$ = require('gulp-load-plugins')(),
	assets = JSON.parse(fs.readFileSync('./paths.json')),
	browserSync = require('browser-sync'),
	src = [].concat("app/assets/scripts/lib/*.js", assets.scripts.src);

gulp.task(scriptsNoJq);

function scriptsNoJq() {
	return gulp.src(src)
		.pipe($.plumber())
		.pipe($.concat('main.nojq.js'))
		.pipe($.uglify())
		.pipe(gulp.dest(assets.scripts.dest))
		.pipe(browserSync.stream());
}