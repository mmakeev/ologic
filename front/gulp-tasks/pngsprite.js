var gulp = require('gulp'),
	fs = require('fs'),
	assets = JSON.parse(fs.readFileSync('./paths.json')),
	$ = require('gulp-load-plugins')(),
	spritesmith = require('gulp.spritesmith'),
	merge = require('merge-stream');

gulp.task(pngsprite);

function pngsprite() {
	var spriteData = gulp.src(assets.images.sprites)
		.pipe(spritesmith({
			imgName: 'sprite.png',
			imgPath: '../img/sprite.png',
			// retinaImgName: 'sprite@2x.png',
			// retinaImgPath: '../img/sprite@2x.png',
			// retinaSrcFilter: 'app/assets/images/sprite/*@2x.png',
			cssName: 'sprite.sass'
		}));
	var imgStream = spriteData.img.pipe(gulp.dest(assets.images.dest));
	var cssStream = spriteData.css.pipe(gulp.dest('app/assets/styles/global/'));

	return merge(imgStream, cssStream);
}
