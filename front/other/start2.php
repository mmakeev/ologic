<!DOCTYPE html>
<html lang="ru">
  <head>
    <style>
      .thanks-youtube {
        position: relative;
        max-width: 1000px;
        margin: 25px auto;
        padding-bottom: 53%;
        background: #000;
      }

      .thanks-youtube iframe {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
      }

      @media screen and (max-width: 767px) {
        .thanks-youtube {
        padding-bottom: 55%;
        }
      }
    </style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta property="og:type" content="article">
    <script src="//unpkg.com/@textback/notification-widget@latest/build/index.js"></script><!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src="https://www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);})(window,document,"script","dataLayer","GTM-T8FCGH8");</script><!-- End Google Tag Manager -->
    <link rel="apple-touch-icon" sizes="180x180" href="./assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="./assets/img/favicon/manifest.json">
    <link rel="mask-icon" href="./assets/img/favicon/safari-pinned-tab.svg" color="#09631d">
    <link rel="shortcut icon" href="./assets/img/favicon/favicon.ico">
    <meta name="msapplication-config" content="./assets/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="./assets/css/main.css?v=1b40227b68d0">
    <title>Онлайн Логика</title>
  </head>
  <!-- Google Tag Manager (noscript) --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T8FCGH8"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <div class="ologic-header">
      <div class="ologic-header__content">
        <h1 class="ologic-header__logo"><a href="./">
            <svg class="ologic-header__globe">
              <use xlink:href="#icon-main-globe"></use>
            </svg><span>Онлайн <br>логика</span></a></h1>
        <nav class="ologic-header__menu ologic-menu">
          <button class="ologic-menu__close" data-close-mobile-menu></button>
          <ul class="ologic-menu__list">
            <li class="ologic-menu__item"><a class="ologic-menu__link" href="experts-list.php">Эксперты</a></li>
            <li class="ologic-menu__item"><a class="ologic-menu__link" href="blog-list.php">Блоги</a></li>
            <li class="ologic-menu__item"><a class="ologic-menu__link" href="timetable.php">Расписание</a></li>
            <li class="ologic-menu__item"><a class="ologic-menu__link" href="start.php">Стать экспертом</a></li>
            <li class="ologic-menu__item ologic-menu__item_login"><a class="ologic-menu__link" href="#ologic-login" data-login-link>
                <svg class="ologic-menu__icon ologic-menu__icon_user">
                  <use xlink:href="#icon-user"></use>
                </svg><span>Вход</span></a></li>
          </ul>
        </nav>
        <button class="ologic-header__menu-button" data-open-mobile-menu></button>
      </div>
    </div>

  <div class="ologic-blog-list" style="
  text-align: center;
  font-size: 27px;
  ">
    <p>Приглашение на вебинар 16 декабря Вам пришлёт LogicMan.</p>
    <p>А сейчас посмотрите это видео:</p>
    <div class="thanks-youtube">
      <iframe src="https://www.youtube.com/embed/RPoSRR8dWZw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>


    <div class="ologic-new-footer">
      <div class="ologic-new-footer__content"><a class="ologic-new-footer__logo" href="./">
          <svg class="ologic-header__globe" width="97" height="89">
            <use xlink:href="#icon-main-globe"></use>
          </svg><span>Онлайн <br>логика</span></a>
        <div class="ologic-new-footer__copyrights">
          <p>Copyright © 2018 «Онлайн-Логика»</p>
          <p>Все права защищены.</p>
        </div>
      </div>
    </div>
    <div class="text-popup" id="video-text-popup">
      <div class="text-popup__content">
        <p>Мы пришлём вам это видео сразу же после подписки на любой из мессенджеров.</p>
      </div>
    </div>
    <form class="ologic-login" id="ologic-login" action="./" method="post">
      <div class="ologic-login__content">
        <h5 class="ologic-login__title">Вход</h5>
        <input class="ologic-login__input" type="text" name="ologic-name" placeholder="E-mail или логин">
        <input class="ologic-login__input" type="password" name="ologic-pass" placeholder="Пароль">
        <input id="ologic-login-remember" type="checkbox" name="ologic-login-remember">
        <label class="ologic-login__label" for="ologic-login-remember">Запомнить меня</label>
        <button class="ologic-login__submit" type="submit">Войти</button>
        <div class="ologic-login__forgot"><a href="#">Забыл пароль</a></div>
        <button class="ologic-login__close" type="button" data-close-login></button>
      </div>
    <script src="./assets/js/main.js?v=1b40227b68d0"></script>
  </body>
</html>